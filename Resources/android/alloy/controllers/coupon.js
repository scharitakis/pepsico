function Controller() {
    function toggleIndicator() {
        if (indicator) {
            $.disableViewBgLoadingIndicator.setVisible(false);
            $.activityIndicator.hide();
            indicator = false;
        } else {
            $.disableViewBgLoadingIndicator.setVisible(true);
            $.activityIndicator.setStyle(Titanium.UI.ActivityIndicatorStyle.BIG);
            $.activityIndicator.show();
            indicator = true;
        }
    }
    function toggleErrorMsg(msg) {
        if ("" != msg) {
            $.errorMsgView.setVisible(true);
            $.errorMsgView.setHeight(Titanium.UI.SIZE);
            $.errorMsgText.setText(msg);
        } else {
            $.errorMsgView.setVisible(false);
            $.errorMsgView.setHeight("0dp");
            $.errorMsgText.setText("");
        }
    }
    function openPopupMsgView() {}
    function closePopupMsgView() {}
    function redeemSuccessClb() {
        var data = JSON.parse(this.responseText);
        toggleIndicator();
        "SUCCESS" == data.result ? data.isBigPrizeWinning ? Ti.App.fireEvent("changeView", {
            row: {
                name: "Winner",
                viewsrc: "winnerView",
                props: data,
                scroll: false
            }
        }) : Ti.App.fireEvent("changeView", {
            row: {
                name: "No Winner",
                viewsrc: "nowinnerView",
                props: data,
                scroll: false
            }
        }) : toggleErrorMsg(data.replyText);
    }
    function redeemErrorClb() {
        toggleIndicator();
        toggleErrorMsg("A Problem occured when trying to Redeem. Please try again");
    }
    function sendPinSuccessClb() {
        var data = JSON.parse(this.responseText);
        toggleIndicator();
        "SUCCESS" == data.result && Ti.App.fireEvent("changeView", {
            row: {
                name: "Pin",
                viewsrc: "pinDialog",
                props: {
                    user: false
                },
                scroll: false
            }
        });
    }
    function sendPinErrorClb() {
        toggleErrorMsg("A Problem Occured. Please try again");
    }
    function msisdnCheckSuccessClb() {
        var data = JSON.parse(this.responseText);
        var msisdn = $.msisdnField.getValue();
        if (data.msisdnRegistered) {
            if (Alloy.Globals.online) {
                var msisdn = $.msisdnField.getValue();
                var coupon = $.couponField.getValue();
                Alloy.Globals.userMsisdn = msisdn;
                var url = Alloy.Globals.serverUrl + "redeemCoupon?coupon=" + coupon + "&msisdn=" + msisdn + "&channel=MOBILE_APP&userAgent=MOBILE_APP&locale=null";
                Alloy.Globals.doGET(url, redeemSuccessClb, redeemErrorClb);
            }
        } else if (Alloy.Globals.online) {
            var url = Alloy.Globals.serverUrl + "sendConfirmationCode?msisdn=" + msisdn + "&confirmationChannel=MOBILE_APP";
            Alloy.Globals.userMsisdn = msisdn;
            Alloy.Globals.doGET(url, sendPinSuccessClb, sendPinErrorClb);
        }
    }
    function msisdnCheckErrorClb() {
        toggleIndicator();
        toggleErrorMsg("Server error");
    }
    function redeemCoupon() {
        $.msisdnField.blur();
        $.couponField.blur();
        toggleErrorMsg("");
        if (Alloy.Globals.online) {
            var msisdnReg = /^[0-9]{4,15}$/;
            var msisdn = $.msisdnField.getValue();
            var coupon = $.couponField.getValue();
            var formValid = true;
            var errorMsg = "";
            if ("" == msisdn) {
                errorMsg = "Please fill in you Mobile Phone";
                formValid = false;
            } else if (msisdnReg.test(msisdn)) {
                if ("" == coupon) {
                    errorMsg = "Please fill in the Coupon Code";
                    formValid = false;
                }
            } else {
                errorMsg = "Invalid input: MSISDN";
                formValid = false;
            }
            if (formValid) {
                toggleIndicator();
                var url = Alloy.Globals.serverUrl + "isMsisdnRegistered?msisdn=" + msisdn;
                Alloy.Globals.doGET(url, msisdnCheckSuccessClb, msisdnCheckErrorClb);
            } else toggleErrorMsg(errorMsg);
        }
    }
    function showLoginPage() {
        Ti.App.fireEvent("changeView", {
            row: {
                name: "Login",
                viewsrc: "login",
                props: null,
                scroll: true
            }
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "coupon";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.coupon = Ti.UI.createView({
        id: "coupon",
        height: "100%",
        width: "100%"
    });
    $.__views.coupon && $.addTopLevelView($.__views.coupon);
    $.__views.__alloyId5 = Ti.UI.createScrollView({
        showVerticalScrollIndicator: "true",
        scrollType: "vertical",
        height: "100%",
        width: "100%",
        backgroundColor: "white",
        contentHeight: "auto",
        id: "__alloyId5"
    });
    $.__views.coupon.add($.__views.__alloyId5);
    $.__views.__alloyId6 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        top: "0",
        id: "__alloyId6"
    });
    $.__views.__alloyId5.add($.__views.__alloyId6);
    $.__views.__alloyId7 = Ti.UI.createImageView({
        image: "/images/bannerCode.png",
        width: "100%",
        id: "__alloyId7"
    });
    $.__views.__alloyId6.add($.__views.__alloyId7);
    $.__views.__alloyId8 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        id: "__alloyId8"
    });
    $.__views.__alloyId6.add($.__views.__alloyId8);
    $.__views.__alloyId9 = Ti.UI.createView({
        height: Titanium.UI.SIZE,
        id: "__alloyId9"
    });
    $.__views.__alloyId8.add($.__views.__alloyId9);
    $.__views.__alloyId10 = Ti.UI.createView({
        layout: "horizontal",
        height: Titanium.UI.SIZE,
        left: "10%",
        right: "10%",
        top: "20",
        id: "__alloyId10"
    });
    $.__views.__alloyId9.add($.__views.__alloyId10);
    $.__views.__alloyId11 = Ti.UI.createImageView({
        image: "/images/capBig.png",
        width: "20%",
        id: "__alloyId11"
    });
    $.__views.__alloyId10.add($.__views.__alloyId11);
    $.__views.codeLabel = Ti.UI.createLabel({
        font: {
            fontFamily: "FuturaStd-Bold",
            fontSize: "18dp"
        },
        color: "#00245E",
        text: "ENTER YOUR CODE",
        id: "codeLabel",
        left: "10",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
        height: "40dp"
    });
    $.__views.__alloyId10.add($.__views.codeLabel);
    $.__views.errorMsgView = Ti.UI.createView({
        visible: "false",
        id: "errorMsgView",
        backgroundColor: "red",
        layout: "horizontal",
        width: "90%",
        height: "0"
    });
    $.__views.__alloyId8.add($.__views.errorMsgView);
    $.__views.errorMsgText = Ti.UI.createLabel({
        id: "errorMsgText"
    });
    $.__views.errorMsgView.add($.__views.errorMsgText);
    $.__views.__alloyId12 = Ti.UI.createView({
        layout: "vertical",
        width: "90%",
        height: Titanium.UI.SIZE,
        id: "__alloyId12"
    });
    $.__views.__alloyId8.add($.__views.__alloyId12);
    $.__views.__alloyId13 = Ti.UI.createView({
        width: "100%",
        height: Titanium.UI.SIZE,
        top: "10dp",
        backgroundColor: "#EAEAEA",
        borderColor: "black",
        borderWidth: "1",
        id: "__alloyId13"
    });
    $.__views.__alloyId12.add($.__views.__alloyId13);
    $.__views.msisdnField = Ti.UI.createTextField({
        left: "5",
        right: "5",
        id: "msisdnField",
        backgroundColor: "#EAEAEA",
        hintText: "Enter your MSISDN",
        color: "#336699",
        height: "40dp"
    });
    $.__views.__alloyId13.add($.__views.msisdnField);
    $.__views.__alloyId14 = Ti.UI.createView({
        width: "100%",
        height: Titanium.UI.SIZE,
        top: "10dp",
        backgroundColor: "#EAEAEA",
        borderColor: "black",
        borderWidth: "1",
        id: "__alloyId14"
    });
    $.__views.__alloyId12.add($.__views.__alloyId14);
    $.__views.couponField = Ti.UI.createTextField({
        left: "5",
        right: "5",
        id: "couponField",
        backgroundColor: "#EAEAEA",
        hintText: "Code",
        color: "#336699",
        height: "40dp"
    });
    $.__views.__alloyId14.add($.__views.couponField);
    $.__views.submitBtn = Ti.UI.createLabel({
        font: {
            fontFamily: "FuturaStd-Bold",
            fontSize: "20dp"
        },
        color: "white",
        text: "SUBMIT",
        id: "submitBtn",
        bottom: "10dp",
        top: "10dp",
        backgroundColor: "#D42220",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
        width: "100%",
        height: "40dp"
    });
    $.__views.__alloyId12.add($.__views.submitBtn);
    redeemCoupon ? $.__views.submitBtn.addEventListener("click", redeemCoupon) : __defers["$.__views.submitBtn!click!redeemCoupon"] = true;
    $.__views.__alloyId15 = Ti.UI.createView({
        visible: "false",
        enabled: "false",
        layout: "vertical",
        height: "0",
        backgroundColor: "#1092C3",
        id: "__alloyId15"
    });
    $.__views.__alloyId6.add($.__views.__alloyId15);
    $.__views.__alloyId16 = Ti.UI.createLabel({
        text: "MORE CHANCES TO WIN!",
        id: "__alloyId16"
    });
    $.__views.__alloyId15.add($.__views.__alloyId16);
    $.__views.__alloyId17 = Ti.UI.createView({
        layout: "horizontal",
        height: "45dp",
        width: "165dp",
        id: "__alloyId17"
    });
    $.__views.__alloyId15.add($.__views.__alloyId17);
    $.__views.__alloyId18 = Ti.UI.createLabel({
        text: "Login",
        backgroundColor: "#00245E",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
        width: "80dp",
        height: "40dp",
        id: "__alloyId18"
    });
    $.__views.__alloyId17.add($.__views.__alloyId18);
    showLoginPage ? $.__views.__alloyId18.addEventListener("click", showLoginPage) : __defers["$.__views.__alloyId18!click!showLoginPage"] = true;
    $.__views.__alloyId19 = Ti.UI.createView({
        width: "5dp",
        id: "__alloyId19"
    });
    $.__views.__alloyId17.add($.__views.__alloyId19);
    $.__views.__alloyId20 = Ti.UI.createLabel({
        text: "SIGN UP",
        backgroundColor: "#27A55C",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
        width: "80dp",
        height: "40dp",
        id: "__alloyId20"
    });
    $.__views.__alloyId17.add($.__views.__alloyId20);
    openPopupMsgView ? $.__views.__alloyId20.addEventListener("click", openPopupMsgView) : __defers["$.__views.__alloyId20!click!openPopupMsgView"] = true;
    $.__views.popUpView = Ti.UI.createView({
        visible: "false",
        id: "popUpView",
        zIndex: "19",
        height: "100%",
        width: "100%",
        bottom: "0",
        backgroundColor: "transparent"
    });
    $.__views.coupon.add($.__views.popUpView);
    $.__views.disableViewBg = Ti.UI.createView({
        id: "disableViewBg",
        top: "0",
        zIndex: "20",
        height: "100%",
        width: "100%",
        bottom: "0",
        backgroundColor: "black",
        opacity: "0.6"
    });
    $.__views.popUpView.add($.__views.disableViewBg);
    $.__views.PopupMsgView = Ti.UI.createView({
        id: "PopupMsgView",
        zIndex: "21",
        height: "90%",
        width: "90%",
        backgroundColor: "white",
        layout: "vertical"
    });
    $.__views.popUpView.add($.__views.PopupMsgView);
    $.__views.__alloyId21 = Ti.UI.createLabel({
        text: "Close (X)",
        right: "0",
        id: "__alloyId21"
    });
    $.__views.PopupMsgView.add($.__views.__alloyId21);
    closePopupMsgView ? $.__views.__alloyId21.addEventListener("click", closePopupMsgView) : __defers["$.__views.__alloyId21!click!closePopupMsgView"] = true;
    $.__views.__alloyId22 = Ti.UI.createScrollView({
        showVerticalScrollIndicator: "true",
        scrollType: "vertical",
        height: "100%",
        width: "100%",
        backgroundColor: "white",
        contentHeight: "auto",
        id: "__alloyId22"
    });
    $.__views.PopupMsgView.add($.__views.__alloyId22);
    $.__views.__alloyId23 = Ti.UI.createView({
        height: "500",
        backgroundColor: "red",
        id: "__alloyId23"
    });
    $.__views.__alloyId22.add($.__views.__alloyId23);
    $.__views.disableViewBgLoadingIndicator = Ti.UI.createView({
        visible: "false",
        id: "disableViewBgLoadingIndicator",
        top: "0",
        zIndex: "20",
        height: "100%",
        width: "100%",
        bottom: "0",
        backgroundColor: "black",
        opacity: "0.7"
    });
    $.__views.coupon.add($.__views.disableViewBgLoadingIndicator);
    $.__views.activityIndicator = Ti.UI.createActivityIndicator({
        zIndex: "21",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        id: "activityIndicator"
    });
    $.__views.coupon.add($.__views.activityIndicator);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var indicator = false;
    if (Ti.App.Properties.hasProperty("user")) {
        $.msisdnField.setValue(Ti.App.Properties.getObject("user")["msisdn"]);
        $.msisdnField.enabled = false;
    } else $.msisdnField.enabled = true;
    Ti.App.addEventListener("view:coupon", function(e) {
        Ti.API.info("fired event view:coupon ------Captured");
        Ti.API.info(e);
        toggleErrorMsg("");
        $.couponField.setValue("");
        if (Ti.App.Properties.hasProperty("user")) {
            $.msisdnField.setValue(Ti.App.Properties.getObject("user")["msisdn"]);
            $.msisdnField.enabled = false;
        } else {
            $.msisdnField.enabled = true;
            $.msisdnField.setValue("");
        }
    });
    __defers["$.__views.submitBtn!click!redeemCoupon"] && $.__views.submitBtn.addEventListener("click", redeemCoupon);
    __defers["$.__views.__alloyId18!click!showLoginPage"] && $.__views.__alloyId18.addEventListener("click", showLoginPage);
    __defers["$.__views.__alloyId20!click!openPopupMsgView"] && $.__views.__alloyId20.addEventListener("click", openPopupMsgView);
    __defers["$.__views.__alloyId21!click!closePopupMsgView"] && $.__views.__alloyId21.addEventListener("click", closePopupMsgView);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;