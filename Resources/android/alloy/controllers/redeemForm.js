function Controller() {
    function toggleErrorMsg(msg) {
        if ("" != msg) {
            $.errorMsgView.setVisible(true);
            $.errorMsgView.setHeight(Titanium.UI.SIZE);
            $.errorMsgText.setText(msg);
        } else {
            $.errorMsgView.setVisible(false);
            $.errorMsgView.setHeight("0dp");
            $.errorMsgText.setText("");
        }
    }
    function deliverySubmit() {
        toggleErrorMsg("");
        var formValid = true;
        var errorMsg = "";
        var nameField = $.nameField.getValue();
        var surnameField = $.surnameField.getValue();
        var mobileNField = $.mobileNField.getValue();
        var addressField = $.addressField.getValue();
        var streetNField = $.streetNField.getValue();
        var cityField = $.cityField.getValue();
        var postCodeField = $.postCodeField.getValue();
        if ("" == nameField) {
            errorMsg = "Invalid input: Name";
            formValid = false;
        }
        if ("" == surnameField && formValid) {
            errorMsg = "Invalid input: Surname";
            formValid = false;
        }
        if ("" == addressField && formValid) {
            errorMsg = "Invalid input: Street";
            formValid = false;
        }
        if ("" == streetNField && formValid) {
            errorMsg = "Invalid input: Street Number";
            formValid = false;
        }
        if ("" == cityField && formValid) {
            errorMsg = "Invalid input: City";
            formValid = false;
        }
        if ("" == postCodeField && formValid) {
            errorMsg = "Invalid input: PostCode";
            formValid = false;
        }
        if ("" == mobileNField && formValid) {
            errorMsg = "Invalid input: Mobile Number";
            formValid = false;
        }
        if (formValid) {
            $.nameField.blur();
            $.surnameField.blur();
            $.mobileNField.blur();
            $.addressField.blur();
            $.streetNField.blur();
            $.cityField.blur();
            $.postCodeField.blur();
            RedeemFormData.nameField = nameField;
            RedeemFormData.surnameField = surnameField;
            RedeemFormData.mobileNField = mobileNField;
            RedeemFormData.addressField = addressField;
            RedeemFormData.streetNField = streetNField;
            RedeemFormData.cityField = cityField;
            RedeemFormData.postCodeField = postCodeField;
            Ti.App.fireEvent("changeView", {
                row: {
                    name: "Review",
                    viewsrc: "reviewRedeemForm",
                    props: RedeemFormData,
                    scroll: false
                }
            });
        } else toggleErrorMsg(errorMsg);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "redeemForm";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.redeemForm = Ti.UI.createScrollView({
        id: "redeemForm",
        showVerticalScrollIndicator: "true",
        scrollType: "vertical",
        height: "100%",
        width: "100%",
        backgroundImage: "/images/backgroundDelivery.png",
        contentHeight: "auto"
    });
    $.__views.redeemForm && $.addTopLevelView($.__views.redeemForm);
    $.__views.__alloyId124 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        top: "0",
        id: "__alloyId124"
    });
    $.__views.redeemForm.add($.__views.__alloyId124);
    $.__views.__alloyId125 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        id: "__alloyId125"
    });
    $.__views.__alloyId124.add($.__views.__alloyId125);
    $.__views.__alloyId126 = Ti.UI.createView({
        layout: "vertical",
        width: "90%",
        height: Titanium.UI.SIZE,
        id: "__alloyId126"
    });
    $.__views.__alloyId125.add($.__views.__alloyId126);
    $.__views.__alloyId127 = Ti.UI.createLabel({
        text: "ENTER BELOW YOUR PERSONAL DELIVERY DATA TO SEND YOU YOUR REWARD!",
        left: "10dp",
        right: "10dp",
        bottom: "10dp",
        id: "__alloyId127"
    });
    $.__views.__alloyId126.add($.__views.__alloyId127);
    $.__views.errorMsgView = Ti.UI.createView({
        visible: "false",
        id: "errorMsgView",
        backgroundColor: "red",
        layout: "horizontal",
        width: "90%",
        height: "0"
    });
    $.__views.__alloyId126.add($.__views.errorMsgView);
    $.__views.errorMsgText = Ti.UI.createLabel({
        id: "errorMsgText"
    });
    $.__views.errorMsgView.add($.__views.errorMsgText);
    $.__views.__alloyId128 = Ti.UI.createLabel({
        text: "NAME",
        left: "10dp",
        id: "__alloyId128"
    });
    $.__views.__alloyId126.add($.__views.__alloyId128);
    $.__views.__alloyId129 = Ti.UI.createView({
        left: "10dp",
        right: "10dp",
        height: Titanium.UI.SIZE,
        top: "5dp",
        backgroundColor: "#EAEAEA",
        borderColor: "black",
        borderWidth: "1",
        id: "__alloyId129"
    });
    $.__views.__alloyId126.add($.__views.__alloyId129);
    $.__views.nameField = Ti.UI.createTextField({
        left: "5",
        right: "5",
        id: "nameField",
        backgroundColor: "#EAEAEA",
        color: "#336699",
        height: "40dp"
    });
    $.__views.__alloyId129.add($.__views.nameField);
    $.__views.__alloyId130 = Ti.UI.createLabel({
        text: "NACHNAME",
        left: "10dp",
        id: "__alloyId130"
    });
    $.__views.__alloyId126.add($.__views.__alloyId130);
    $.__views.__alloyId131 = Ti.UI.createView({
        left: "10dp",
        right: "10dp",
        height: Titanium.UI.SIZE,
        top: "5dp",
        backgroundColor: "#EAEAEA",
        borderColor: "black",
        borderWidth: "1",
        id: "__alloyId131"
    });
    $.__views.__alloyId126.add($.__views.__alloyId131);
    $.__views.surnameField = Ti.UI.createTextField({
        left: "5",
        right: "5",
        id: "surnameField",
        backgroundColor: "#EAEAEA",
        color: "#336699",
        height: "40"
    });
    $.__views.__alloyId131.add($.__views.surnameField);
    $.__views.__alloyId132 = Ti.UI.createView({
        height: Titanium.UI.SIZE,
        layout: "horizontal",
        id: "__alloyId132"
    });
    $.__views.__alloyId126.add($.__views.__alloyId132);
    $.__views.__alloyId133 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        width: "60%",
        id: "__alloyId133"
    });
    $.__views.__alloyId132.add($.__views.__alloyId133);
    $.__views.__alloyId134 = Ti.UI.createLabel({
        text: "STREET ADDRESS",
        left: "10dp",
        id: "__alloyId134"
    });
    $.__views.__alloyId133.add($.__views.__alloyId134);
    $.__views.__alloyId135 = Ti.UI.createView({
        left: "10dp",
        right: "10dp",
        height: Titanium.UI.SIZE,
        top: "5dp",
        backgroundColor: "#EAEAEA",
        borderColor: "black",
        borderWidth: "1",
        id: "__alloyId135"
    });
    $.__views.__alloyId133.add($.__views.__alloyId135);
    $.__views.addressField = Ti.UI.createTextField({
        left: "5",
        right: "5",
        id: "addressField",
        backgroundColor: "#EAEAEA",
        color: "#336699",
        height: "40"
    });
    $.__views.__alloyId135.add($.__views.addressField);
    $.__views.__alloyId136 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        width: "40%",
        id: "__alloyId136"
    });
    $.__views.__alloyId132.add($.__views.__alloyId136);
    $.__views.__alloyId137 = Ti.UI.createLabel({
        text: "NUMBER",
        left: "5dp",
        id: "__alloyId137"
    });
    $.__views.__alloyId136.add($.__views.__alloyId137);
    $.__views.__alloyId138 = Ti.UI.createView({
        left: "5dp",
        right: "10dp",
        height: Titanium.UI.SIZE,
        top: "5dp",
        backgroundColor: "#EAEAEA",
        borderColor: "black",
        borderWidth: "1",
        id: "__alloyId138"
    });
    $.__views.__alloyId136.add($.__views.__alloyId138);
    $.__views.streetNField = Ti.UI.createTextField({
        left: "5",
        right: "5",
        id: "streetNField",
        backgroundColor: "#EAEAEA",
        color: "#336699",
        height: "40"
    });
    $.__views.__alloyId138.add($.__views.streetNField);
    $.__views.__alloyId139 = Ti.UI.createView({
        height: Titanium.UI.SIZE,
        layout: "horizontal",
        id: "__alloyId139"
    });
    $.__views.__alloyId126.add($.__views.__alloyId139);
    $.__views.__alloyId140 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        width: "60%",
        id: "__alloyId140"
    });
    $.__views.__alloyId139.add($.__views.__alloyId140);
    $.__views.__alloyId141 = Ti.UI.createLabel({
        text: "CITY REGION",
        left: "10dp",
        id: "__alloyId141"
    });
    $.__views.__alloyId140.add($.__views.__alloyId141);
    $.__views.__alloyId142 = Ti.UI.createView({
        left: "10dp",
        right: "10dp",
        height: Titanium.UI.SIZE,
        top: "5dp",
        backgroundColor: "#EAEAEA",
        borderColor: "black",
        borderWidth: "1",
        id: "__alloyId142"
    });
    $.__views.__alloyId140.add($.__views.__alloyId142);
    $.__views.cityField = Ti.UI.createTextField({
        left: "5",
        right: "5",
        id: "cityField",
        backgroundColor: "#EAEAEA",
        color: "#336699",
        height: "40"
    });
    $.__views.__alloyId142.add($.__views.cityField);
    $.__views.__alloyId143 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        width: "40%",
        id: "__alloyId143"
    });
    $.__views.__alloyId139.add($.__views.__alloyId143);
    $.__views.__alloyId144 = Ti.UI.createLabel({
        text: "POSTAL",
        left: "5dp",
        id: "__alloyId144"
    });
    $.__views.__alloyId143.add($.__views.__alloyId144);
    $.__views.__alloyId145 = Ti.UI.createView({
        left: "5dp",
        right: "10dp",
        height: Titanium.UI.SIZE,
        top: "5dp",
        backgroundColor: "#EAEAEA",
        borderColor: "black",
        borderWidth: "1",
        id: "__alloyId145"
    });
    $.__views.__alloyId143.add($.__views.__alloyId145);
    $.__views.postCodeField = Ti.UI.createTextField({
        left: "5",
        right: "5",
        id: "postCodeField",
        backgroundColor: "#EAEAEA",
        color: "#336699",
        height: "40"
    });
    $.__views.__alloyId145.add($.__views.postCodeField);
    $.__views.__alloyId146 = Ti.UI.createLabel({
        text: "MOBILE",
        left: "10dp",
        id: "__alloyId146"
    });
    $.__views.__alloyId126.add($.__views.__alloyId146);
    $.__views.__alloyId147 = Ti.UI.createView({
        left: "10dp",
        right: "10dp",
        height: Titanium.UI.SIZE,
        top: "5dp",
        backgroundColor: "#EAEAEA",
        borderColor: "black",
        borderWidth: "1",
        id: "__alloyId147"
    });
    $.__views.__alloyId126.add($.__views.__alloyId147);
    $.__views.mobileNField = Ti.UI.createTextField({
        left: "5",
        right: "5",
        id: "mobileNField",
        backgroundColor: "#EAEAEA",
        color: "#336699",
        height: "40"
    });
    $.__views.__alloyId147.add($.__views.mobileNField);
    $.__views.__alloyId148 = Ti.UI.createLabel({
        text: "SUBMIT",
        backgroundColor: "#D42220",
        bottom: "10",
        top: "10",
        left: "10dp",
        right: "10dp",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
        height: "40dp",
        id: "__alloyId148"
    });
    $.__views.__alloyId126.add($.__views.__alloyId148);
    deliverySubmit ? $.__views.__alloyId148.addEventListener("click", deliverySubmit) : __defers["$.__views.__alloyId148!click!deliverySubmit"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    var RedeemFormData = {};
    Ti.App.addEventListener("view:redeemForm", function(e) {
        Ti.API.info("fired event view:redeemForm ------Captured");
        RedeemFormData = e;
        toggleErrorMsg("");
    });
    __defers["$.__views.__alloyId148!click!deliverySubmit"] && $.__views.__alloyId148.addEventListener("click", deliverySubmit);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;