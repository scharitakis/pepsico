function Controller() {
    function loadCallBack() {
        Ti.API.info("Received text: " + this.responseText);
        var data = JSON.parse(this.responseText);
        var aboutMeData = data["aboutMeApiResponseUnAnswered"];
        var socializeData = data["socializeApiResponseUnAnswered"];
        var missionsData = data["missionsApiResponseUnAnswered"];
        var quizVoteData = data["quizVoteQuestionApiResponseUnAnswered"];
        var aboutMeRows = [];
        var socializeRows = [];
        var missionsRows = [];
        var quizVoteRows = [];
        for (var i = 0; aboutMeData.length > i; i++) {
            var aboutMeRow = Ti.UI.createTableViewRow({
                title: aboutMeData[i]["aboutMeActionType"],
                height: 100
            });
            aboutMeRows.push(aboutMeRow);
        }
        for (var d = 0; quizVoteData.length > d; d++) {
            var quizVoteRow = Ti.UI.createTableViewRow({
                title: quizVoteData[d]["questionText"],
                height: 100
            });
            quizVoteRows.push(quizVoteRow);
        }
        for (var f = 0; socializeData.length > f; f++) {
            var socializeRow = Ti.UI.createTableViewRow({
                title: socializeData[f]["socializeActionType"],
                height: 100
            });
            socializeRows.push(socializeRow);
        }
        for (var g = 0; missionsData.length > g; g++) {
            if ("LIKE_FACEBOOK_PAGE" != missionsData[g].missionActionType) var missionsRow = Ti.UI.createTableViewRow({
                title: missionsData[g]["title"],
                description: missionsData[g]["description"],
                linkurl: missionsData[g]["url"],
                selectedColor: "transparent",
                height: 100
            });
            missionsRows.push(missionsRow);
        }
        $.activitiesTable.removeAllChildren();
        var tableData = aboutMeRows.concat(socializeRows, missionsRows, quizVoteRows);
        $.activitiesTable.setData(tableData);
    }
    function errCallBack(e) {
        Ti.API.debug(e.error);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "activities";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.activities = Ti.UI.createView({
        id: "activities"
    });
    $.__views.activities && $.addTopLevelView($.__views.activities);
    $.__views.activitiesTable = Ti.UI.createTableView({
        id: "activitiesTable"
    });
    $.__views.activities.add($.__views.activitiesTable);
    exports.destroy = function() {};
    _.extend($, $.__views);
    Ti.App.addEventListener("view:activities", function(e) {
        Ti.API.info("fired event view:activities ------Captured");
        Ti.API.info(e);
        if (Alloy.Globals.online) {
            var url = Alloy.Globals.serverUrl + "loadRestMembersUnAnsweredQuestions?msisdn=79500000304&channel=MOBILE_APP";
            Alloy.Globals.doGET(url, loadCallBack, errCallBack);
        }
    });
    $.activitiesTable.addEventListener("click", function(e) {
        Ti.API.info("Activity row clicked" + e);
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;