function Controller() {
    function Logoff() {
        Ti.App.Properties.removeProperty("user");
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "settings";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.settings = Ti.UI.createView({
        id: "settings"
    });
    $.__views.settings && $.addTopLevelView($.__views.settings);
    $.__views.__alloyId167 = Ti.UI.createLabel({
        text: "Logoff",
        id: "__alloyId167"
    });
    $.__views.settings.add($.__views.__alloyId167);
    Logoff ? $.__views.__alloyId167.addEventListener("click", Logoff) : __defers["$.__views.__alloyId167!click!Logoff"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    Ti.App.addEventListener("view:settings", function(e) {
        Ti.API.info("fired event view:settings ------Captured");
        Ti.API.info(e);
    });
    __defers["$.__views.__alloyId167!click!Logoff"] && $.__views.__alloyId167.addEventListener("click", Logoff);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;