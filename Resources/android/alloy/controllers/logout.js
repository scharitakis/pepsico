function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "logout";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.logout = Ti.UI.createView({
        id: "logout"
    });
    $.__views.logout && $.addTopLevelView($.__views.logout);
    exports.destroy = function() {};
    _.extend($, $.__views);
    Ti.App.addEventListener("view:pinDialog", function(e) {
        Ti.API.info("fired event view:pinDialog ------Captured");
        Ti.API.info(e);
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;