function Controller() {
    function toggleIndicator() {
        if (indicator) {
            $.disableViewBgLoadingIndicator.setVisible(false);
            $.activityIndicator.hide();
            indicator = false;
        } else {
            $.disableViewBgLoadingIndicator.setVisible(true);
            $.activityIndicator.setStyle(Titanium.UI.ActivityIndicatorStyle.BIG);
            $.activityIndicator.show();
            indicator = true;
        }
    }
    function toggleErrorMsg(msg) {
        if ("" != msg) {
            $.errorMsgView.setVisible(true);
            $.errorMsgView.setHeight(Titanium.UI.SIZE);
            $.errorMsgText.setText(msg);
        } else {
            $.errorMsgView.setVisible(false);
            $.errorMsgView.setHeight("0dp");
            $.errorMsgText.setText("");
        }
    }
    function successPinClb() {
        var data = JSON.parse(this.responseText);
        toggleIndicator();
        if ("SUCCESS" == data.result) {
            var pinVal = $.pinField.getValue();
            Ti.App.Properties.setObject("user", {
                msisdn: Alloy.Globals.userMsisdn,
                pin: pinVal
            });
            if (goToForm) Ti.App.fireEvent("changeView", {
                row: {
                    name: "Delivery",
                    viewsrc: "redeemForm",
                    props: WinnerData,
                    scroll: false
                }
            }); else {
                var goBackView = Alloy.Globals.previousView;
                goBackView.row.scroll = false;
                Ti.App.fireEvent("changeView", goBackView);
            }
        } else "INVALID_CONFIRMATION_CODE" == data.failReason && toggleErrorMsg("The pin You entered is wrong, Please check again");
    }
    function errorPinClb() {
        toggleIndicator();
        toggleErrorMsg("Server error");
    }
    function submitPin() {
        $.pinField.blur();
        toggleErrorMsg("");
        if (Alloy.Globals.online) {
            toggleIndicator();
            var msisdn = Alloy.Globals.userMsisdn;
            var pinVal = $.pinField.getValue();
            var url = Alloy.Globals.serverUrl + "confirmMsisdn?msisdn=" + msisdn + "&confirmationChannel=MOBILE_APP&confirmationCode=" + pinVal + "&registrationSite=MOBILE_APP&userAgent=MOBILE_APP&pushToken=" + Alloy.Globals.PushToken + "&device=" + Alloy.Globals.isiOS;
            Alloy.Globals.doGET(url, successPinClb, errorPinClb);
        }
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "pinDialog";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.pinDialog = Ti.UI.createView({
        id: "pinDialog",
        height: "100%",
        width: "100%"
    });
    $.__views.pinDialog && $.addTopLevelView($.__views.pinDialog);
    $.__views.__alloyId94 = Ti.UI.createScrollView({
        showVerticalScrollIndicator: "true",
        scrollType: "vertical",
        height: "100%",
        width: "100%",
        backgroundColor: "white",
        contentHeight: "auto",
        id: "__alloyId94"
    });
    $.__views.pinDialog.add($.__views.__alloyId94);
    $.__views.__alloyId95 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        top: "0",
        id: "__alloyId95"
    });
    $.__views.__alloyId94.add($.__views.__alloyId95);
    $.__views.__alloyId96 = Ti.UI.createImageView({
        image: "/images/bannerCode.png",
        width: "100%",
        id: "__alloyId96"
    });
    $.__views.__alloyId95.add($.__views.__alloyId96);
    $.__views.__alloyId97 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        id: "__alloyId97"
    });
    $.__views.__alloyId95.add($.__views.__alloyId97);
    $.__views.__alloyId98 = Ti.UI.createView({
        height: Titanium.UI.SIZE,
        id: "__alloyId98"
    });
    $.__views.__alloyId97.add($.__views.__alloyId98);
    $.__views.__alloyId99 = Ti.UI.createView({
        layout: "horizontal",
        height: Titanium.UI.SIZE,
        left: "10%",
        right: "10%",
        top: "20",
        id: "__alloyId99"
    });
    $.__views.__alloyId98.add($.__views.__alloyId99);
    $.__views.__alloyId100 = Ti.UI.createImageView({
        image: "/images/capBig.png",
        width: "20%",
        id: "__alloyId100"
    });
    $.__views.__alloyId99.add($.__views.__alloyId100);
    $.__views.codeLabel = Ti.UI.createLabel({
        text: "ENTER YOUR PIN",
        id: "codeLabel",
        left: "10",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
        height: "40dp"
    });
    $.__views.__alloyId99.add($.__views.codeLabel);
    $.__views.errorMsgView = Ti.UI.createView({
        visible: "false",
        id: "errorMsgView",
        backgroundColor: "red",
        layout: "horizontal",
        width: "90%",
        height: "0"
    });
    $.__views.__alloyId97.add($.__views.errorMsgView);
    $.__views.errorMsgText = Ti.UI.createLabel({
        id: "errorMsgText"
    });
    $.__views.errorMsgView.add($.__views.errorMsgText);
    $.__views.__alloyId101 = Ti.UI.createView({
        layout: "vertical",
        width: "90%",
        height: Titanium.UI.SIZE,
        id: "__alloyId101"
    });
    $.__views.__alloyId97.add($.__views.__alloyId101);
    $.__views.__alloyId102 = Ti.UI.createView({
        width: "100%",
        height: Titanium.UI.SIZE,
        top: "10dp",
        backgroundColor: "#EAEAEA",
        borderColor: "black",
        borderWidth: "1",
        id: "__alloyId102"
    });
    $.__views.__alloyId101.add($.__views.__alloyId102);
    $.__views.pinField = Ti.UI.createTextField({
        left: "5",
        id: "pinField",
        backgroundColor: "#EAEAEA",
        hintText: "Code",
        color: "#336699",
        width: "100%",
        height: "40dp"
    });
    $.__views.__alloyId102.add($.__views.pinField);
    $.__views.__alloyId103 = Ti.UI.createLabel({
        text: "SUBMIT",
        top: "30dp",
        backgroundColor: "#D42220",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
        width: "100%",
        height: "40dp",
        id: "__alloyId103"
    });
    $.__views.__alloyId101.add($.__views.__alloyId103);
    submitPin ? $.__views.__alloyId103.addEventListener("click", submitPin) : __defers["$.__views.__alloyId103!click!submitPin"] = true;
    $.__views.disableViewBgLoadingIndicator = Ti.UI.createView({
        visible: "false",
        id: "disableViewBgLoadingIndicator",
        top: "0",
        zIndex: "20",
        height: "100%",
        width: "100%",
        bottom: "0",
        backgroundColor: "black",
        opacity: "0.7"
    });
    $.__views.pinDialog.add($.__views.disableViewBgLoadingIndicator);
    $.__views.activityIndicator = Ti.UI.createActivityIndicator({
        zIndex: "21",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        id: "activityIndicator"
    });
    $.__views.pinDialog.add($.__views.activityIndicator);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var indicator = false;
    var WinnerData = {};
    var goToForm = false;
    Ti.App.addEventListener("view:pinDialog", function(e) {
        WinnerData = {};
        goToForm = false;
        Ti.API.info("fired event view:pinDialog ------Captured");
        toggleErrorMsg("");
        if (e.user) {
            WinnerData = e.data;
            goToForm = true;
        }
    });
    __defers["$.__views.__alloyId103!click!submitPin"] && $.__views.__alloyId103.addEventListener("click", submitPin);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;