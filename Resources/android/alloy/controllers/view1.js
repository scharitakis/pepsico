function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "view1";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.view1 = Ti.UI.createView({
        id: "view1"
    });
    $.__views.view1 && $.addTopLevelView($.__views.view1);
    $.__views.__alloyId4 = Ti.UI.createLabel({
        text: "Home",
        id: "__alloyId4"
    });
    $.__views.view1.add($.__views.__alloyId4);
    exports.destroy = function() {};
    _.extend($, $.__views);
    Ti.App.addEventListener("sliderToggled", function(e) {
        e.hasSlided;
    });
    var fb = require("facebook");
    fb.appid = "213868488820229";
    fb.permissions = [ "publish_stream", "publish_actions", "read_friendlists" ];
    fb.forceDialogAuth = true;
    fb.addEventListener("login", function(e) {
        if (e.success) {
            var ss = fb.getAccessToken();
            alert(ss);
            alert("Logged in");
        }
    });
    fb.addEventListener("logout", function() {
        Ti.Network.createHTTPClient().clearCookies("https://m.facebook.com");
        Ti.Network.createHTTPClient().clearCookies("http://m.facebook.com");
        alert("Logged out");
    });
    $.view1.add(fb.createLoginButton({
        top: 50,
        style: fb.BUTTON_STYLE_WIDE
    }));
    var logoutFBbtn = Ti.UI.createButton({
        title: "logout",
        width: 70,
        height: 40,
        left: 0,
        top: 110
    });
    logoutFBbtn.addEventListener("click", function() {
        fb.logout();
    });
    $.view1.add(logoutFBbtn);
    var propertiesBtn = Ti.UI.createButton({
        title: "getProperties",
        width: 70,
        height: 40,
        left: 70,
        top: 110
    });
    propertiesBtn.addEventListener("click", function() {
        var properties = Ti.App.Properties.listProperties();
        var s = "";
        for (var i = 0, ilen = properties.length; ilen > i; i++) {
            var value = Ti.App.Properties.getString(properties[i]);
            Ti.API.info(properties[i] + " = " + value);
            s = s + "-------" + properties[i] + " = " + value;
        }
        alert(s);
        var aa = Ti.App.Properties.getString("FBAccessToken");
        alert(aa);
    });
    $.view1.add(propertiesBtn);
    alert(Ti.App.Properties.listProperties());
    var getOnFBbtn = Ti.UI.createButton({
        title: "FB GET",
        width: 70,
        height: 40,
        left: 0,
        top: 80
    });
    getOnFBbtn.addEventListener("click", function() {
        fb.requestWithGraphPath("me", {}, "GET", function(e) {
            e.success ? alert(e.result) : e.error ? alert(e.error) : alert("Unknown response");
        });
    });
    $.view1.add(getOnFBbtn);
    var postOnFBbtn = Ti.UI.createButton({
        title: "FB Post",
        width: 70,
        height: 40,
        left: 70,
        top: 80
    });
    postOnFBbtn.addEventListener("click", function() {
        var data = {
            message: "This is a test message"
        };
        fb.requestWithGraphPath("me/feed", data, "POST", function(e) {
            e.success ? alert(e.result) : e.error ? alert(e.error) : alert("Unknown response");
        });
    });
    $.view1.add(postOnFBbtn);
    var inviteOnFBbtn = Ti.UI.createButton({
        title: "FB Invite",
        width: 70,
        height: 40,
        left: 140,
        top: 80
    });
    inviteOnFBbtn.addEventListener("click", function() {
        fb.dialog("apprequests", {
            message: "my custom message"
        }, function(data) {
            Ti.API.info(data);
        });
    });
    $.view1.add(inviteOnFBbtn);
    var postOnFBDbtn = Ti.UI.createButton({
        title: "FB Post Dialog",
        width: 70,
        height: 40,
        left: 210,
        top: 80
    });
    postOnFBDbtn.addEventListener("click", function() {
        var data = {
            link: "http://www.appcelerator.com",
            name: "Appcelerator Titanium Mobile",
            message: "Checkout this cool open source project for creating mobile apps",
            caption: "Appcelerator Titanium Mobile",
            picture: "http://developer.appcelerator.com/assets/img/DEV_titmobile_image.png",
            description: "You've got the ideas, now you've got the power. Titanium translates your hard won web skills into native applications..."
        };
        fb.dialog("feed", data, function(e) {
            e.success && e.result ? alert("Success! New Post ID: " + e.result) : e.error ? alert(e.error) : alert("User canceled dialog.");
        });
    });
    $.view1.add(postOnFBDbtn);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;