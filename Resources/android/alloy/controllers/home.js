function Controller() {
    function doCoupon() {
        Ti.App.fireEvent("changeView", {
            row: {
                name: "Coupon",
                viewsrc: "coupon",
                props: null,
                scroll: false
            }
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "home";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.home = Ti.UI.createView({
        id: "home",
        height: "100%",
        width: "100%"
    });
    $.__views.home && $.addTopLevelView($.__views.home);
    $.__views.__alloyId27 = Ti.UI.createScrollView({
        showVerticalScrollIndicator: "true",
        scrollType: "vertical",
        height: "100%",
        width: "100%",
        backgroundColor: "white",
        contentHeight: "auto",
        id: "__alloyId27"
    });
    $.__views.home.add($.__views.__alloyId27);
    $.__views.__alloyId28 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        top: "0",
        id: "__alloyId28"
    });
    $.__views.__alloyId27.add($.__views.__alloyId28);
    $.__views.__alloyId29 = Ti.UI.createImageView({
        image: "/images/bannerHome.png",
        width: "100%",
        id: "__alloyId29"
    });
    $.__views.__alloyId28.add($.__views.__alloyId29);
    $.__views.__alloyId30 = Ti.UI.createView({
        height: "100dp",
        backgroundColor: "red",
        id: "__alloyId30"
    });
    $.__views.__alloyId28.add($.__views.__alloyId30);
    doCoupon ? $.__views.__alloyId30.addEventListener("click", doCoupon) : __defers["$.__views.__alloyId30!click!doCoupon"] = true;
    $.__views.__alloyId31 = Ti.UI.createView({
        layout: "horizontal",
        height: Titanium.UI.SIZE,
        left: "10",
        id: "__alloyId31"
    });
    $.__views.__alloyId30.add($.__views.__alloyId31);
    $.__views.__alloyId32 = Ti.UI.createImageView({
        image: "/images/capBig.png",
        width: "20%",
        id: "__alloyId32"
    });
    $.__views.__alloyId31.add($.__views.__alloyId32);
    $.__views.codeLabel = Ti.UI.createLabel({
        font: {
            fontFamily: "FuturaStd-Bold",
            fontSize: "20dp"
        },
        color: "white",
        text: "ENTER YOUR CODE",
        id: "codeLabel",
        left: "10",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
        height: "40dp"
    });
    $.__views.__alloyId31.add($.__views.codeLabel);
    $.__views.disableViewBgLoadingIndicator = Ti.UI.createView({
        visible: "false",
        id: "disableViewBgLoadingIndicator",
        top: "0",
        zIndex: "20",
        height: "100%",
        width: "100%",
        bottom: "0",
        backgroundColor: "black",
        opacity: "0.7"
    });
    $.__views.home.add($.__views.disableViewBgLoadingIndicator);
    $.__views.activityIndicator = Ti.UI.createActivityIndicator({
        zIndex: "21",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        id: "activityIndicator"
    });
    $.__views.home.add($.__views.activityIndicator);
    exports.destroy = function() {};
    _.extend($, $.__views);
    Ti.App.addEventListener("view:home", function() {});
    __defers["$.__views.__alloyId30!click!doCoupon"] && $.__views.__alloyId30.addEventListener("click", doCoupon);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;