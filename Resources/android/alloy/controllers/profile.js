function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "profile";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.profile = Ti.UI.createView({
        id: "profile"
    });
    $.__views.profile && $.addTopLevelView($.__views.profile);
    var __alloyId105 = [];
    $.__views.sectionFruit = Ti.UI.createTableViewSection({
        id: "sectionFruit",
        headerTitle: "Fruit"
    });
    __alloyId105.push($.__views.sectionFruit);
    $.__views.__alloyId106 = Ti.UI.createTableViewRow({
        title: "Apple",
        id: "__alloyId106"
    });
    $.__views.sectionFruit.add($.__views.__alloyId106);
    $.__views.__alloyId107 = Ti.UI.createTableViewRow({
        title: "Bananas",
        id: "__alloyId107"
    });
    $.__views.sectionFruit.add($.__views.__alloyId107);
    $.__views.sectionVeg = Ti.UI.createTableViewSection({
        id: "sectionVeg",
        headerTitle: "Vegetables"
    });
    __alloyId105.push($.__views.sectionVeg);
    $.__views.__alloyId108 = Ti.UI.createTableViewRow({
        title: "Carrots",
        id: "__alloyId108"
    });
    $.__views.sectionVeg.add($.__views.__alloyId108);
    $.__views.__alloyId109 = Ti.UI.createTableViewRow({
        title: "Potatoes",
        id: "__alloyId109"
    });
    $.__views.sectionVeg.add($.__views.__alloyId109);
    $.__views.sectionFish = Ti.UI.createTableViewSection({
        id: "sectionFish",
        headerTitle: "Fish"
    });
    __alloyId105.push($.__views.sectionFish);
    $.__views.__alloyId110 = Ti.UI.createTableViewRow({
        title: "Cod",
        id: "__alloyId110"
    });
    $.__views.sectionFish.add($.__views.__alloyId110);
    $.__views.__alloyId111 = Ti.UI.createTableViewRow({
        title: "Haddock",
        id: "__alloyId111"
    });
    $.__views.sectionFish.add($.__views.__alloyId111);
    $.__views.sectionFish = Ti.UI.createTableViewSection({
        id: "sectionFish",
        headerTitle: "Fish"
    });
    __alloyId105.push($.__views.sectionFish);
    $.__views.__alloyId112 = Ti.UI.createTableViewRow({
        title: "Cod",
        id: "__alloyId112"
    });
    $.__views.sectionFish.add($.__views.__alloyId112);
    $.__views.__alloyId113 = Ti.UI.createTableViewRow({
        title: "Haddock",
        id: "__alloyId113"
    });
    $.__views.sectionFish.add($.__views.__alloyId113);
    $.__views.sectionFish = Ti.UI.createTableViewSection({
        id: "sectionFish",
        headerTitle: "Fish"
    });
    __alloyId105.push($.__views.sectionFish);
    $.__views.__alloyId114 = Ti.UI.createTableViewRow({
        title: "Cod",
        id: "__alloyId114"
    });
    $.__views.sectionFish.add($.__views.__alloyId114);
    $.__views.__alloyId115 = Ti.UI.createTableViewRow({
        title: "Haddock",
        id: "__alloyId115"
    });
    $.__views.sectionFish.add($.__views.__alloyId115);
    $.__views.sectionFish = Ti.UI.createTableViewSection({
        id: "sectionFish",
        headerTitle: "Fish"
    });
    __alloyId105.push($.__views.sectionFish);
    $.__views.__alloyId116 = Ti.UI.createTableViewRow({
        title: "Cod",
        id: "__alloyId116"
    });
    $.__views.sectionFish.add($.__views.__alloyId116);
    $.__views.__alloyId117 = Ti.UI.createTableViewRow({
        title: "Haddock",
        id: "__alloyId117"
    });
    $.__views.sectionFish.add($.__views.__alloyId117);
    $.__views.sectionFish = Ti.UI.createTableViewSection({
        id: "sectionFish",
        headerTitle: "Fish"
    });
    __alloyId105.push($.__views.sectionFish);
    $.__views.__alloyId118 = Ti.UI.createTableViewRow({
        title: "Cod",
        id: "__alloyId118"
    });
    $.__views.sectionFish.add($.__views.__alloyId118);
    $.__views.__alloyId119 = Ti.UI.createTableViewRow({
        title: "Haddock",
        id: "__alloyId119"
    });
    $.__views.sectionFish.add($.__views.__alloyId119);
    $.__views.sectionFish = Ti.UI.createTableViewSection({
        id: "sectionFish",
        headerTitle: "Fish"
    });
    __alloyId105.push($.__views.sectionFish);
    $.__views.__alloyId120 = Ti.UI.createTableViewRow({
        title: "Cod",
        id: "__alloyId120"
    });
    $.__views.sectionFish.add($.__views.__alloyId120);
    $.__views.__alloyId121 = Ti.UI.createTableViewRow({
        title: "Haddock",
        id: "__alloyId121"
    });
    $.__views.sectionFish.add($.__views.__alloyId121);
    $.__views.sectionFish = Ti.UI.createTableViewSection({
        id: "sectionFish",
        headerTitle: "Fish"
    });
    __alloyId105.push($.__views.sectionFish);
    $.__views.__alloyId122 = Ti.UI.createTableViewRow({
        title: "Cod",
        id: "__alloyId122"
    });
    $.__views.sectionFish.add($.__views.__alloyId122);
    $.__views.__alloyId123 = Ti.UI.createTableViewRow({
        title: "Haddock",
        id: "__alloyId123"
    });
    $.__views.sectionFish.add($.__views.__alloyId123);
    $.__views.table = Ti.UI.createTableView({
        data: __alloyId105,
        id: "table"
    });
    $.__views.profile.add($.__views.table);
    exports.destroy = function() {};
    _.extend($, $.__views);
    Ti.App.addEventListener("view:profile", function(e) {
        Ti.API.info("fired event view:profile ------Captured");
        Ti.API.info(e);
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;