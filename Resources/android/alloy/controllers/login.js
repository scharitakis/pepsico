function Controller() {
    function test() {
        Alloy.Globals.setupUI(false);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "login";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.login = Ti.UI.createScrollView({
        id: "login",
        showVerticalScrollIndicator: "true",
        scrollType: "vertical",
        height: "100%",
        width: "100%",
        backgroundColor: "white",
        contentHeight: "auto"
    });
    $.__views.login && $.addTopLevelView($.__views.login);
    $.__views.__alloyId66 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        top: "0",
        id: "__alloyId66"
    });
    $.__views.login.add($.__views.__alloyId66);
    $.__views.__alloyId67 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        id: "__alloyId67"
    });
    $.__views.__alloyId66.add($.__views.__alloyId67);
    $.__views.__alloyId68 = Ti.UI.createLabel({
        text: "Connect",
        height: "20dp",
        left: "5%",
        id: "__alloyId68"
    });
    $.__views.__alloyId67.add($.__views.__alloyId68);
    $.__views.__alloyId69 = Ti.UI.createView({
        layout: "vertical",
        width: "90%",
        backgroundColor: "#008fd1",
        height: Titanium.UI.SIZE,
        id: "__alloyId69"
    });
    $.__views.__alloyId67.add($.__views.__alloyId69);
    $.__views.__alloyId70 = Ti.UI.createLabel({
        text: "Sign in with Facebook",
        width: "90%",
        id: "__alloyId70"
    });
    $.__views.__alloyId69.add($.__views.__alloyId70);
    $.__views.__alloyId71 = Ti.UI.createLabel({
        text: "Sign in with Google",
        width: "90%",
        id: "__alloyId71"
    });
    $.__views.__alloyId69.add($.__views.__alloyId71);
    $.__views.__alloyId72 = Ti.UI.createLabel({
        text: "Pepsi Connect",
        width: "90%",
        id: "__alloyId72"
    });
    $.__views.__alloyId69.add($.__views.__alloyId72);
    $.__views.__alloyId73 = Ti.UI.createButton({
        title: "test",
        id: "__alloyId73"
    });
    $.__views.__alloyId69.add($.__views.__alloyId73);
    test ? $.__views.__alloyId73.addEventListener("click", test) : __defers["$.__views.__alloyId73!click!test"] = true;
    $.__views.__alloyId74 = Ti.UI.createView({
        height: "20dp",
        id: "__alloyId74"
    });
    $.__views.__alloyId67.add($.__views.__alloyId74);
    $.__views.__alloyId75 = Ti.UI.createView({
        height: "1",
        backgroundColor: "#008fd1",
        id: "__alloyId75"
    });
    $.__views.__alloyId67.add($.__views.__alloyId75);
    $.__views.__alloyId76 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        id: "__alloyId76"
    });
    $.__views.__alloyId66.add($.__views.__alloyId76);
    $.__views.__alloyId77 = Ti.UI.createLabel({
        text: "Login",
        height: "20dp",
        left: "5%",
        id: "__alloyId77"
    });
    $.__views.__alloyId76.add($.__views.__alloyId77);
    $.__views.__alloyId78 = Ti.UI.createView({
        layout: "vertical",
        width: "90%",
        backgroundColor: "#008fd1",
        height: Titanium.UI.SIZE,
        id: "__alloyId78"
    });
    $.__views.__alloyId76.add($.__views.__alloyId78);
    $.__views.msisdnField = Ti.UI.createTextField({
        id: "msisdnField",
        backgroundColor: "#EAEAEA",
        hintText: "Enter your MSISDN",
        color: "#336699",
        top: "10",
        left: "10",
        width: "90%",
        height: "40"
    });
    $.__views.__alloyId78.add($.__views.msisdnField);
    $.__views.passField = Ti.UI.createTextField({
        id: "passField",
        backgroundColor: "#EAEAEA",
        hintText: "Enter your Password",
        color: "#336699",
        top: "10",
        left: "10",
        width: "90%",
        height: "40"
    });
    $.__views.__alloyId78.add($.__views.passField);
    $.__views.__alloyId79 = Ti.UI.createLabel({
        text: "Submit",
        id: "__alloyId79"
    });
    $.__views.__alloyId78.add($.__views.__alloyId79);
    $.__views.__alloyId80 = Ti.UI.createLabel({
        text: "FORGOT YOUR PASSWORD",
        id: "__alloyId80"
    });
    $.__views.__alloyId78.add($.__views.__alloyId80);
    $.__views.__alloyId81 = Ti.UI.createView({
        height: "20dp",
        id: "__alloyId81"
    });
    $.__views.__alloyId76.add($.__views.__alloyId81);
    $.__views.__alloyId82 = Ti.UI.createView({
        height: "1",
        backgroundColor: "#008fd1",
        id: "__alloyId82"
    });
    $.__views.__alloyId76.add($.__views.__alloyId82);
    $.__views.__alloyId83 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        backgroundColor: "#1092C3",
        id: "__alloyId83"
    });
    $.__views.__alloyId66.add($.__views.__alloyId83);
    $.__views.__alloyId84 = Ti.UI.createLabel({
        text: "DONT HAVE AN ACCOUNT YET",
        id: "__alloyId84"
    });
    $.__views.__alloyId83.add($.__views.__alloyId84);
    $.__views.__alloyId85 = Ti.UI.createLabel({
        text: "SIGN UP",
        id: "__alloyId85"
    });
    $.__views.__alloyId83.add($.__views.__alloyId85);
    exports.destroy = function() {};
    _.extend($, $.__views);
    Ti.App.addEventListener("view:login", function(e) {
        Ti.API.info("fired event view:login ------Captured");
        Ti.API.info(e);
    });
    __defers["$.__views.__alloyId73!click!test"] && $.__views.__alloyId73.addEventListener("click", test);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;