function Controller() {
    function doChange() {
        Ti.App.fireEvent("changeView", {
            row: {
                name: "Home",
                viewsrc: "home",
                props: null,
                scroll: false
            }
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "redeemSuccess";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.redeemSuccess = Ti.UI.createView({
        id: "redeemSuccess",
        height: "100%",
        width: "100%",
        backgroundColor: "red"
    });
    $.__views.redeemSuccess && $.addTopLevelView($.__views.redeemSuccess);
    $.__views.__alloyId149 = Ti.UI.createScrollView({
        showVerticalScrollIndicator: "true",
        scrollType: "vertical",
        height: "100%",
        width: "100%",
        backgroundImage: "/images/backgroundDelivery.png",
        contentHeight: "auto",
        id: "__alloyId149"
    });
    $.__views.redeemSuccess.add($.__views.__alloyId149);
    $.__views.__alloyId150 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        top: "0",
        width: "90%",
        id: "__alloyId150"
    });
    $.__views.__alloyId149.add($.__views.__alloyId150);
    $.__views.__alloyId151 = Ti.UI.createImageView({
        image: "/images/delivery_icon.png",
        width: "50%",
        top: "20dp",
        id: "__alloyId151"
    });
    $.__views.__alloyId150.add($.__views.__alloyId151);
    $.__views.l1 = Ti.UI.createLabel({
        font: {
            fontSize: "20dp",
            fontFamily: "FuturaStd-Bold"
        },
        color: "#004C94",
        text: "Danke!",
        id: "l1",
        top: "20dp"
    });
    $.__views.__alloyId150.add($.__views.l1);
    $.__views.l2 = Ti.UI.createLabel({
        font: {
            fontSize: "16dp",
            fontFamily: "FuturaStd-Bold"
        },
        color: "#004C94",
        text: "Prize redemption successfully completed!",
        id: "l2",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
        top: "10dp"
    });
    $.__views.__alloyId150.add($.__views.l2);
    $.__views.l3 = Ti.UI.createLabel({
        font: {
            fontSize: "16dp",
            fontFamily: "FuturaStd-Bold"
        },
        color: "#004C94",
        text: "We will soon ship your prize to the address you have provided.",
        id: "l3",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER
    });
    $.__views.__alloyId150.add($.__views.l3);
    $.__views.goBackBtn = Ti.UI.createLabel({
        font: {
            fontSize: "18dp",
            fontFamily: "FuturaStd-Bold"
        },
        color: "white",
        text: "ENTER NEW CODE",
        top: "10",
        id: "goBackBtn",
        backgroundColor: "#015CB5",
        bottom: "10",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
        width: "70%",
        height: "40dp"
    });
    $.__views.__alloyId150.add($.__views.goBackBtn);
    doChange ? $.__views.goBackBtn.addEventListener("click", doChange) : __defers["$.__views.goBackBtn!click!doChange"] = true;
    $.__views.disableViewBgLoadingIndicator = Ti.UI.createView({
        visible: "false",
        id: "disableViewBgLoadingIndicator",
        top: "0",
        zIndex: "20",
        height: "100%",
        width: "100%",
        bottom: "0",
        backgroundColor: "black",
        opacity: "0.7"
    });
    $.__views.redeemSuccess.add($.__views.disableViewBgLoadingIndicator);
    $.__views.activityIndicator = Ti.UI.createActivityIndicator({
        zIndex: "21",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        id: "activityIndicator"
    });
    $.__views.redeemSuccess.add($.__views.activityIndicator);
    exports.destroy = function() {};
    _.extend($, $.__views);
    Ti.App.addEventListener("view:redeemSuccess", function() {
        Ti.API.info("fired event view:redeemSuccess ------Captured");
    });
    __defers["$.__views.goBackBtn!click!doChange"] && $.__views.goBackBtn.addEventListener("click", doChange);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;