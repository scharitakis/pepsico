function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "howtoplay";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.howtoplay = Ti.UI.createView({
        id: "howtoplay",
        height: "100%",
        width: "100%"
    });
    $.__views.howtoplay && $.addTopLevelView($.__views.howtoplay);
    var __alloyId33 = [];
    $.__views.__alloyId34 = Ti.UI.createScrollView({
        showVerticalScrollIndicator: "true",
        scrollType: "vertical",
        height: "100%",
        backgroundColor: "#008DBD",
        width: "100%",
        contentHeight: "auto",
        id: "__alloyId34"
    });
    __alloyId33.push($.__views.__alloyId34);
    $.__views.__alloyId35 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        top: "0",
        width: "90%",
        id: "__alloyId35"
    });
    $.__views.__alloyId34.add($.__views.__alloyId35);
    $.__views.__alloyId36 = Ti.UI.createImageView({
        top: "30dp",
        image: "/images/how_step1_icon.png",
        width: "50%",
        id: "__alloyId36"
    });
    $.__views.__alloyId35.add($.__views.__alloyId36);
    $.__views.__alloyId37 = Ti.UI.createLabel({
        text: "STEP 1",
        top: "20dp",
        id: "__alloyId37"
    });
    $.__views.__alloyId35.add($.__views.__alloyId37);
    $.__views.__alloyId38 = Ti.UI.createLabel({
        text: "BUY THE PEPSI BOTTLES",
        top: "20dp",
        id: "__alloyId38"
    });
    $.__views.__alloyId35.add($.__views.__alloyId38);
    $.__views.__alloyId39 = Ti.UI.createLabel({
        text: "WITH THE GOLDEN CAPS",
        id: "__alloyId39"
    });
    $.__views.__alloyId35.add($.__views.__alloyId39);
    $.__views.__alloyId40 = Ti.UI.createLabel({
        text: "(1.0LT AND 1.5LT)",
        id: "__alloyId40"
    });
    $.__views.__alloyId35.add($.__views.__alloyId40);
    $.__views.__alloyId41 = Ti.UI.createLabel({
        text: "WITH THE SPECIAL CREATIVE.",
        id: "__alloyId41"
    });
    $.__views.__alloyId35.add($.__views.__alloyId41);
    $.__views.__alloyId42 = Ti.UI.createScrollView({
        showVerticalScrollIndicator: "true",
        scrollType: "vertical",
        height: "100%",
        width: "100%",
        backgroundColor: "#008DBD",
        contentHeight: "auto",
        id: "__alloyId42"
    });
    __alloyId33.push($.__views.__alloyId42);
    $.__views.__alloyId43 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        top: "0",
        width: "90%",
        id: "__alloyId43"
    });
    $.__views.__alloyId42.add($.__views.__alloyId43);
    $.__views.__alloyId44 = Ti.UI.createImageView({
        top: "30dp",
        image: "/images/how_step1_icon.png",
        width: "50%",
        id: "__alloyId44"
    });
    $.__views.__alloyId43.add($.__views.__alloyId44);
    $.__views.__alloyId45 = Ti.UI.createLabel({
        text: "STEP 2",
        top: "20dp",
        id: "__alloyId45"
    });
    $.__views.__alloyId43.add($.__views.__alloyId45);
    $.__views.__alloyId46 = Ti.UI.createLabel({
        text: "BUY THE PEPSI BOTTLES",
        top: "20dp",
        id: "__alloyId46"
    });
    $.__views.__alloyId43.add($.__views.__alloyId46);
    $.__views.__alloyId47 = Ti.UI.createLabel({
        text: "WITH THE GOLDEN CAPS",
        id: "__alloyId47"
    });
    $.__views.__alloyId43.add($.__views.__alloyId47);
    $.__views.__alloyId48 = Ti.UI.createLabel({
        text: "(1.0LT AND 1.5LT)",
        id: "__alloyId48"
    });
    $.__views.__alloyId43.add($.__views.__alloyId48);
    $.__views.__alloyId49 = Ti.UI.createLabel({
        text: "WITH THE SPECIAL CREATIVE.",
        id: "__alloyId49"
    });
    $.__views.__alloyId43.add($.__views.__alloyId49);
    $.__views.__alloyId50 = Ti.UI.createScrollView({
        showVerticalScrollIndicator: "true",
        scrollType: "vertical",
        height: "100%",
        width: "100%",
        backgroundColor: "#008DBD",
        contentHeight: "auto",
        id: "__alloyId50"
    });
    __alloyId33.push($.__views.__alloyId50);
    $.__views.__alloyId51 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        top: "0",
        width: "90%",
        id: "__alloyId51"
    });
    $.__views.__alloyId50.add($.__views.__alloyId51);
    $.__views.__alloyId52 = Ti.UI.createImageView({
        top: "30dp",
        image: "/images/how_step1_icon.png",
        width: "50%",
        id: "__alloyId52"
    });
    $.__views.__alloyId51.add($.__views.__alloyId52);
    $.__views.__alloyId53 = Ti.UI.createLabel({
        text: "STEP 3",
        top: "20dp",
        id: "__alloyId53"
    });
    $.__views.__alloyId51.add($.__views.__alloyId53);
    $.__views.__alloyId54 = Ti.UI.createLabel({
        text: "BUY THE PEPSI BOTTLES",
        top: "20dp",
        id: "__alloyId54"
    });
    $.__views.__alloyId51.add($.__views.__alloyId54);
    $.__views.__alloyId55 = Ti.UI.createLabel({
        text: "WITH THE GOLDEN CAPS",
        id: "__alloyId55"
    });
    $.__views.__alloyId51.add($.__views.__alloyId55);
    $.__views.__alloyId56 = Ti.UI.createLabel({
        text: "(1.0LT AND 1.5LT)",
        id: "__alloyId56"
    });
    $.__views.__alloyId51.add($.__views.__alloyId56);
    $.__views.__alloyId57 = Ti.UI.createLabel({
        text: "WITH THE SPECIAL CREATIVE.",
        id: "__alloyId57"
    });
    $.__views.__alloyId51.add($.__views.__alloyId57);
    $.__views.__alloyId58 = Ti.UI.createScrollView({
        showVerticalScrollIndicator: "true",
        scrollType: "vertical",
        height: "100%",
        width: "100%",
        backgroundColor: "#008DBD",
        contentHeight: "auto",
        id: "__alloyId58"
    });
    __alloyId33.push($.__views.__alloyId58);
    $.__views.__alloyId59 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        top: "0",
        width: "90%",
        id: "__alloyId59"
    });
    $.__views.__alloyId58.add($.__views.__alloyId59);
    $.__views.__alloyId60 = Ti.UI.createImageView({
        top: "30dp",
        image: "/images/how_step1_icon.png",
        width: "50%",
        id: "__alloyId60"
    });
    $.__views.__alloyId59.add($.__views.__alloyId60);
    $.__views.__alloyId61 = Ti.UI.createLabel({
        text: "STEP 4",
        top: "20dp",
        id: "__alloyId61"
    });
    $.__views.__alloyId59.add($.__views.__alloyId61);
    $.__views.__alloyId62 = Ti.UI.createLabel({
        text: "BUY THE PEPSI BOTTLES",
        top: "20dp",
        id: "__alloyId62"
    });
    $.__views.__alloyId59.add($.__views.__alloyId62);
    $.__views.__alloyId63 = Ti.UI.createLabel({
        text: "WITH THE GOLDEN CAPS",
        id: "__alloyId63"
    });
    $.__views.__alloyId59.add($.__views.__alloyId63);
    $.__views.__alloyId64 = Ti.UI.createLabel({
        text: "(1.0LT AND 1.5LT)",
        id: "__alloyId64"
    });
    $.__views.__alloyId59.add($.__views.__alloyId64);
    $.__views.__alloyId65 = Ti.UI.createLabel({
        text: "WITH THE SPECIAL CREATIVE.",
        id: "__alloyId65"
    });
    $.__views.__alloyId59.add($.__views.__alloyId65);
    $.__views.scrollableView = Ti.UI.createScrollableView({
        views: __alloyId33,
        id: "scrollableView",
        showPagingControl: "false",
        backgroundColor: "#008DBD"
    });
    $.__views.howtoplay.add($.__views.scrollableView);
    $.__views.disableViewBgLoadingIndicator = Ti.UI.createView({
        visible: "false",
        id: "disableViewBgLoadingIndicator",
        top: "0",
        zIndex: "20",
        height: "100%",
        width: "100%",
        bottom: "0",
        backgroundColor: "black",
        opacity: "0.7"
    });
    $.__views.howtoplay.add($.__views.disableViewBgLoadingIndicator);
    $.__views.activityIndicator = Ti.UI.createActivityIndicator({
        zIndex: "21",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        id: "activityIndicator"
    });
    $.__views.howtoplay.add($.__views.activityIndicator);
    exports.destroy = function() {};
    _.extend($, $.__views);
    Ti.App.addEventListener("view:howtoplay", function(e) {
        Ti.API.info("fired event view:howtoplay ------Captured");
        Ti.API.info(e);
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;