function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "rightMenuRow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.tablerowr = Ti.UI.createTableViewRow({
        id: "tablerowr",
        width: "250dp",
        height: "41dp",
        backgroundSelectedColor: "white",
        layout: "vertical"
    });
    $.__views.tablerowr && $.addTopLevelView($.__views.tablerowr);
    $.__views.tablerowrView = Ti.UI.createView({
        id: "tablerowrView",
        height: "40dp",
        width: "250dp"
    });
    $.__views.tablerowr.add($.__views.tablerowrView);
    $.__views.tablerowrLabel = Ti.UI.createLabel({
        font: {
            fontSize: "16dp",
            fontFamily: "FuturaStd-Bold"
        },
        color: "white",
        left: "20dp",
        id: "tablerowrLabel",
        height: "40dp"
    });
    $.__views.tablerowrView.add($.__views.tablerowrLabel);
    $.__views.__alloyId166 = Ti.UI.createView({
        height: "1",
        backgroundColor: "#013797",
        width: "250dp",
        id: "__alloyId166"
    });
    $.__views.tablerowr.add($.__views.__alloyId166);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    $.tablerowrLabel.text = args.name || "";
    $.tablerowr.name = args.name || "";
    $.tablerowr.viewsrc = args.viewsrc || "";
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;