function Controller() {
    function doClose() {
        Ti.App.fireEvent("changeView", {
            row: {
                name: "Home",
                viewsrc: "home",
                props: null,
                scroll: false
            }
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "nowinnerView";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.nowinnerView = Ti.UI.createView({
        id: "nowinnerView",
        height: "100%",
        width: "100%"
    });
    $.__views.nowinnerView && $.addTopLevelView($.__views.nowinnerView);
    $.__views.winnerScrollView = Ti.UI.createScrollView({
        id: "winnerScrollView",
        showVerticalScrollIndicator: "true",
        scrollType: "vertical",
        height: "100%",
        width: "100%",
        backgroundImage: "/images/backgroundDelivery.png",
        contentHeight: "auto"
    });
    $.__views.nowinnerView.add($.__views.winnerScrollView);
    $.__views.winnerViewNoPrize = Ti.UI.createView({
        id: "winnerViewNoPrize",
        width: "80%",
        layout: "vertical",
        height: Titanium.UI.SIZE,
        top: "0"
    });
    $.__views.winnerScrollView.add($.__views.winnerViewNoPrize);
    $.__views.__alloyId86 = Ti.UI.createView({
        height: Titanium.UI.SIZE,
        top: "20dp",
        id: "__alloyId86"
    });
    $.__views.winnerViewNoPrize.add($.__views.__alloyId86);
    $.__views.__alloyId87 = Ti.UI.createView({
        layout: "horizontal",
        height: Titanium.UI.SIZE,
        width: Titanium.UI.SIZE,
        id: "__alloyId87"
    });
    $.__views.__alloyId86.add($.__views.__alloyId87);
    $.__views.__alloyId88 = Ti.UI.createLabel({
        text: "+",
        id: "__alloyId88"
    });
    $.__views.__alloyId87.add($.__views.__alloyId88);
    $.__views.pointAwardedL = Ti.UI.createLabel({
        id: "pointAwardedL"
    });
    $.__views.__alloyId87.add($.__views.pointAwardedL);
    $.__views.__alloyId89 = Ti.UI.createLabel({
        text: "Punkte",
        id: "__alloyId89"
    });
    $.__views.__alloyId87.add($.__views.__alloyId89);
    $.__views.totalPointsL = Ti.UI.createLabel({
        text: "Für die Wochen ß und Überraschungsziehungen",
        top: "10dp",
        id: "totalPointsL"
    });
    $.__views.winnerViewNoPrize.add($.__views.totalPointsL);
    $.__views.__alloyId90 = Ti.UI.createView({
        top: "20dp",
        backgroundColor: "#008DBA",
        width: "100%",
        height: Titanium.UI.SIZE,
        id: "__alloyId90"
    });
    $.__views.winnerViewNoPrize.add($.__views.__alloyId90);
    $.__views.__alloyId91 = Ti.UI.createView({
        top: "5dp",
        bottom: "5dp",
        width: Titanium.UI.SIZE,
        layout: "horizontal",
        height: Titanium.UI.SIZE,
        id: "__alloyId91"
    });
    $.__views.__alloyId90.add($.__views.__alloyId91);
    $.__views.__alloyId92 = Ti.UI.createLabel({
        text: "MEIN SCORE:",
        id: "__alloyId92"
    });
    $.__views.__alloyId91.add($.__views.__alloyId92);
    $.__views.totalPointsL = Ti.UI.createLabel({
        id: "totalPointsL"
    });
    $.__views.__alloyId91.add($.__views.totalPointsL);
    $.__views.replyTextL = Ti.UI.createLabel({
        top: "20dp",
        id: "replyTextL",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER
    });
    $.__views.winnerViewNoPrize.add($.__views.replyTextL);
    $.__views.__alloyId93 = Ti.UI.createLabel({
        text: "ENTER NEW CODE",
        top: "20dp",
        backgroundColor: "#015CB5",
        width: "100%",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
        height: "40dp",
        id: "__alloyId93"
    });
    $.__views.winnerViewNoPrize.add($.__views.__alloyId93);
    doClose ? $.__views.__alloyId93.addEventListener("click", doClose) : __defers["$.__views.__alloyId93!click!doClose"] = true;
    $.__views.disableViewBgLoadingIndicator = Ti.UI.createView({
        visible: "false",
        id: "disableViewBgLoadingIndicator",
        top: "0",
        zIndex: "20",
        height: "100%",
        width: "100%",
        bottom: "0",
        backgroundColor: "black",
        opacity: "0.7"
    });
    $.__views.nowinnerView.add($.__views.disableViewBgLoadingIndicator);
    $.__views.activityIndicator = Ti.UI.createActivityIndicator({
        zIndex: "21",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        id: "activityIndicator"
    });
    $.__views.nowinnerView.add($.__views.activityIndicator);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var data = {};
    Ti.App.addEventListener("view:nowinnerView", function(e) {
        data = e;
        $.pointAwardedL.setText(data.pointsAwarded);
        $.totalPointsL.setText(data.pointsBalance);
        $.replyTextL.setText(data.replyText);
    });
    __defers["$.__views.__alloyId93!click!doClose"] && $.__views.__alloyId93.addEventListener("click", doClose);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;