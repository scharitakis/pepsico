function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "leftMenuRow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.tablerowl = Ti.UI.createTableViewRow({
        id: "tablerowl",
        backgroundSelectedColor: "white"
    });
    $.__views.tablerowl && $.addTopLevelView($.__views.tablerowl);
    $.__views.tablerowlView = Ti.UI.createView({
        id: "tablerowlView"
    });
    $.__views.tablerowl.add($.__views.tablerowlView);
    $.__views.tablerowlLabel = Ti.UI.createLabel({
        font: {
            fontSize: "18dp",
            fontWeight: "bold"
        },
        color: "white",
        id: "tablerowlLabel"
    });
    $.__views.tablerowlView.add($.__views.tablerowlLabel);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    $.tablerowlLabel.text = args.name || "";
    $.tablerowl.name = args.name || "";
    $.tablerowl.viewsrc = args.viewsrc || "";
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;