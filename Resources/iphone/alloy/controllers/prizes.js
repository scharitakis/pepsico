function Controller() {
    function testme1() {
        var data = {
            username: "p02096@gmail.com",
            password: "123456789"
        };
        var url = "http://10.1.6.29:8050/promo/web/proxylogin";
        var loadCallBack = function() {
            alert(this.responseData);
        };
        var errCallBack = function(e) {
            alert("error" + e);
        };
        Alloy.Globals.doPost(data, url, loadCallBack, errCallBack);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "prizes";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.prizes = Ti.UI.createView({
        id: "prizes"
    });
    $.__views.prizes && $.addTopLevelView($.__views.prizes);
    $.__views.__alloyId104 = Ti.UI.createLabel({
        text: "prizes",
        id: "__alloyId104"
    });
    $.__views.prizes.add($.__views.__alloyId104);
    testme1 ? $.__views.__alloyId104.addEventListener("click", testme1) : __defers["$.__views.__alloyId104!click!testme1"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    Ti.App.addEventListener("view:prizes", function(e) {
        Ti.API.info("fired event view:prizes ------Captured");
        Ti.API.info(e);
    });
    __defers["$.__views.__alloyId104!click!testme1"] && $.__views.__alloyId104.addEventListener("click", testme1);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;