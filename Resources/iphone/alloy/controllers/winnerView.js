function Controller() {
    function toggleIndicator() {
        if (indicator) {
            $.disableViewBgLoadingIndicator.setVisible(false);
            $.activityIndicator.hide();
            indicator = false;
        } else {
            $.disableViewBgLoadingIndicator.setVisible(true);
            "iphone" === Ti.Platform.osname ? $.activityIndicator.setStyle(Titanium.UI.iPhone.ActivityIndicatorStyle.BIG) : $.activityIndicator.setStyle(Titanium.UI.ActivityIndicatorStyle.BIG);
            $.activityIndicator.show();
            indicator = true;
        }
    }
    function sendPinWinnerSuccessClb() {
        toggleIndicator();
        "SUCCESS" == data.result && Ti.App.fireEvent("changeView", {
            row: {
                name: "Pin",
                viewsrc: "pinDialog",
                props: {
                    user: true,
                    data: data
                },
                scroll: false
            }
        });
    }
    function sendPinWinnerErrorClb() {
        toggleIndicator();
        toggleErrorMsg("A Problem Occured. Please try again");
    }
    function doRedeemForm() {
        if (Ti.App.Properties.hasProperty("user")) Ti.App.fireEvent("changeView", {
            row: {
                name: "Delivery",
                viewsrc: "redeemForm",
                props: data,
                scroll: false
            }
        }); else if (Alloy.Globals.online) {
            toggleIndicator();
            var msisdn = Alloy.Globals.userMsisdn;
            var url = Alloy.Globals.serverUrl + "sendConfirmationCode?msisdn=" + msisdn + "&confirmationChannel=MOBILE_APP";
            Alloy.Globals.doGET(url, sendPinWinnerSuccessClb, sendPinWinnerErrorClb);
        }
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "winnerView";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.winnerView = Ti.UI.createView({
        id: "winnerView",
        height: "100%",
        width: "100%"
    });
    $.__views.winnerView && $.addTopLevelView($.__views.winnerView);
    $.__views.winnerScrollView = Ti.UI.createScrollView({
        id: "winnerScrollView",
        showVerticalScrollIndicator: "true",
        scrollType: "vertical",
        height: "100%",
        width: "100%",
        contentHeight: "auto"
    });
    $.__views.winnerView.add($.__views.winnerScrollView);
    $.__views.winnerViewBigPrize = Ti.UI.createView({
        id: "winnerViewBigPrize",
        layout: "vertical",
        height: Titanium.UI.SIZE,
        top: "0"
    });
    $.__views.winnerScrollView.add($.__views.winnerViewBigPrize);
    $.__views.__alloyId168 = Ti.UI.createImageView({
        image: "/images/bannerHome.png",
        width: "100%",
        id: "__alloyId168"
    });
    $.__views.winnerViewBigPrize.add($.__views.__alloyId168);
    $.__views.__alloyId169 = Ti.UI.createView({
        layout: "horizontal",
        height: Titanium.UI.SIZE,
        id: "__alloyId169"
    });
    $.__views.winnerViewBigPrize.add($.__views.__alloyId169);
    $.__views.__alloyId170 = Ti.UI.createView({
        layout: "horizontal",
        height: Titanium.UI.SIZE,
        id: "__alloyId170"
    });
    $.__views.__alloyId169.add($.__views.__alloyId170);
    $.__views.__alloyId171 = Ti.UI.createImageView({
        image: "/images/capBig.png",
        width: "20%",
        id: "__alloyId171"
    });
    $.__views.__alloyId170.add($.__views.__alloyId171);
    $.__views.codeLabel = Ti.UI.createLabel({
        text: "It's a GOAL!",
        id: "codeLabel",
        left: "10",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
        height: "40dp"
    });
    $.__views.__alloyId170.add($.__views.codeLabel);
    $.__views.redeemNowBtn = Ti.UI.createLabel({
        text: "REDEEM NOW",
        id: "redeemNowBtn",
        backgroundColor: "#D42220",
        width: "200dp",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
        height: "40dp"
    });
    $.__views.winnerViewBigPrize.add($.__views.redeemNowBtn);
    doRedeemForm ? $.__views.redeemNowBtn.addEventListener("click", doRedeemForm) : __defers["$.__views.redeemNowBtn!click!doRedeemForm"] = true;
    $.__views.disableViewBgLoadingIndicator = Ti.UI.createView({
        visible: "false",
        id: "disableViewBgLoadingIndicator",
        top: "0",
        zIndex: "20",
        height: "100%",
        width: "100%",
        bottom: "0",
        backgroundColor: "black",
        opacity: "0.7"
    });
    $.__views.winnerView.add($.__views.disableViewBgLoadingIndicator);
    $.__views.activityIndicator = Ti.UI.createActivityIndicator({
        zIndex: "21",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        id: "activityIndicator"
    });
    $.__views.winnerView.add($.__views.activityIndicator);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var data = {};
    var indicator = false;
    Ti.App.addEventListener("view:winnerView", function(e) {
        data = e;
    });
    __defers["$.__views.redeemNowBtn!click!doRedeemForm"] && $.__views.redeemNowBtn.addEventListener("click", doRedeemForm);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;