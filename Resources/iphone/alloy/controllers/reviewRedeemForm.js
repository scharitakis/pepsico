function Controller() {
    function toggleIndicator() {
        if (indicator) {
            $.disableViewBgLoadingIndicator.setVisible(false);
            $.activityIndicator.hide();
            indicator = false;
        } else {
            $.disableViewBgLoadingIndicator.setVisible(true);
            "iphone" === Ti.Platform.osname ? $.activityIndicator.setStyle(Titanium.UI.iPhone.ActivityIndicatorStyle.BIG) : $.activityIndicator.setStyle(Titanium.UI.ActivityIndicatorStyle.BIG);
            $.activityIndicator.show();
            indicator = true;
        }
    }
    function toggleErrorMsg(msg) {
        if ("" != msg) {
            $.errorMsgView.setVisible(true);
            $.errorMsgView.setHeight(Titanium.UI.SIZE);
            $.errorMsgText.setText(msg);
        } else {
            $.errorMsgView.setVisible(false);
            $.errorMsgView.setHeight("0dp");
            $.errorMsgText.setText("");
        }
    }
    function toggleIndicator() {
        if (indicator) {
            $.disableViewBgLoadingIndicator.setVisible(false);
            $.activityIndicator.hide();
            indicator = false;
        } else {
            $.disableViewBgLoadingIndicator.setVisible(true);
            "iphone" === Ti.Platform.osname ? $.activityIndicator.setStyle(Titanium.UI.iPhone.ActivityIndicatorStyle.BIG) : $.activityIndicator.setStyle(Titanium.UI.ActivityIndicatorStyle.BIG);
            $.activityIndicator.show();
            indicator = true;
        }
    }
    function doChange() {
        Ti.App.fireEvent("changeView", {
            row: {
                name: "Review",
                viewsrc: "redeemForm",
                props: reviewRedeemFormData,
                scroll: false
            }
        });
    }
    function RedeemFormSuccessClb() {
        toggleIndicator();
        var data = JSON.parse(this.responseText);
        if ("SUCCESS" == data.result) Ti.App.fireEvent("changeView", {
            row: {
                name: "Success",
                viewsrc: "redeemSuccess",
                props: reviewRedeemFormData,
                scroll: false
            }
        }); else {
            toggleErrorMsg("A problem occured");
            alert("failed" + JSON.stringify(data));
        }
    }
    function RedeemFormErrorClb() {
        toggleIndicator();
        toggleErrorMsg("A problem occured");
    }
    function sendDetails() {
        if (Alloy.Globals.online) {
            toggleIndicator();
            var confirmationCode = Ti.App.Properties.getObject("user").pin;
            var url = Alloy.Globals.serverUrl + "submitDeliveryData?memberId=" + reviewRedeemFormData.memberId + "&confirmationCode=" + confirmationCode + "&conversationId=" + reviewRedeemFormData.conversationId + "&name=" + reviewRedeemFormData.nameField + "&surname=" + reviewRedeemFormData.surnameField + "&street=" + reviewRedeemFormData.addressField + "&streetNo=" + reviewRedeemFormData.streetNField + "&postalCode=" + reviewRedeemFormData.postCodeField + "&city=" + reviewRedeemFormData.cityField + "&mobile=" + reviewRedeemFormData.mobileNField + "&channel=MOBILE_APP";
            Alloy.Globals.doGET(url, RedeemFormSuccessClb, RedeemFormErrorClb);
        }
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "reviewRedeemForm";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.reviewRedeemForm = Ti.UI.createView({
        id: "reviewRedeemForm",
        height: "100%",
        width: "100%"
    });
    $.__views.reviewRedeemForm && $.addTopLevelView($.__views.reviewRedeemForm);
    $.__views.__alloyId152 = Ti.UI.createScrollView({
        showVerticalScrollIndicator: "true",
        scrollType: "vertical",
        backgroundImage: "/images/backgroundDelivery.png",
        height: "100%",
        width: "100%",
        contentHeight: "auto",
        id: "__alloyId152"
    });
    $.__views.reviewRedeemForm.add($.__views.__alloyId152);
    $.__views.__alloyId153 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        top: "0",
        width: "90%",
        id: "__alloyId153"
    });
    $.__views.__alloyId152.add($.__views.__alloyId153);
    $.__views.__alloyId154 = Ti.UI.createLabel({
        text: "MAKE SURE THE INFORMATION ENTERED IS CORRECT",
        left: "10dp",
        right: "10dp",
        bottom: "20dp",
        id: "__alloyId154"
    });
    $.__views.__alloyId153.add($.__views.__alloyId154);
    $.__views.__alloyId155 = Ti.UI.createLabel({
        text: "DELIVER MY PRIZE TO THIS ADDRESS",
        left: "10dp",
        right: "10dp",
        id: "__alloyId155"
    });
    $.__views.__alloyId153.add($.__views.__alloyId155);
    $.__views.errorMsgView = Ti.UI.createView({
        visible: "false",
        id: "errorMsgView",
        backgroundColor: "red",
        layout: "horizontal",
        width: "90%",
        height: "0"
    });
    $.__views.__alloyId153.add($.__views.errorMsgView);
    $.__views.errorMsgText = Ti.UI.createLabel({
        id: "errorMsgText"
    });
    $.__views.errorMsgView.add($.__views.errorMsgText);
    $.__views.__alloyId156 = Ti.UI.createView({
        left: "10dp",
        right: "10dp",
        layout: "vertical",
        height: Titanium.UI.SIZE,
        backgroundColor: "blue",
        id: "__alloyId156"
    });
    $.__views.__alloyId153.add($.__views.__alloyId156);
    $.__views.__alloyId157 = Ti.UI.createView({
        left: "10dp",
        right: "10dp",
        top: "10dp",
        layout: "horizontal",
        height: Titanium.UI.SIZE,
        id: "__alloyId157"
    });
    $.__views.__alloyId156.add($.__views.__alloyId157);
    $.__views.nameL = Ti.UI.createLabel({
        id: "nameL",
        color: "white"
    });
    $.__views.__alloyId157.add($.__views.nameL);
    $.__views.surnameL = Ti.UI.createLabel({
        id: "surnameL"
    });
    $.__views.__alloyId157.add($.__views.surnameL);
    $.__views.__alloyId158 = Ti.UI.createView({
        left: "10dp",
        right: "10dp",
        layout: "horizontal",
        height: Titanium.UI.SIZE,
        id: "__alloyId158"
    });
    $.__views.__alloyId156.add($.__views.__alloyId158);
    $.__views.streetL = Ti.UI.createLabel({
        id: "streetL"
    });
    $.__views.__alloyId158.add($.__views.streetL);
    $.__views.streetNL = Ti.UI.createLabel({
        id: "streetNL"
    });
    $.__views.__alloyId158.add($.__views.streetNL);
    $.__views.cityL = Ti.UI.createLabel({
        id: "cityL"
    });
    $.__views.__alloyId158.add($.__views.cityL);
    $.__views.postCL = Ti.UI.createLabel({
        id: "postCL"
    });
    $.__views.__alloyId158.add($.__views.postCL);
    $.__views.__alloyId159 = Ti.UI.createView({
        left: "10dp",
        right: "10dp",
        bottom: "40dp",
        layout: "horizontal",
        height: Titanium.UI.SIZE,
        id: "__alloyId159"
    });
    $.__views.__alloyId156.add($.__views.__alloyId159);
    $.__views.__alloyId160 = Ti.UI.createLabel({
        text: "mob:",
        id: "__alloyId160"
    });
    $.__views.__alloyId159.add($.__views.__alloyId160);
    $.__views.mobL = Ti.UI.createLabel({
        id: "mobL"
    });
    $.__views.__alloyId159.add($.__views.mobL);
    $.__views.__alloyId161 = Ti.UI.createView({
        top: "30dp",
        left: "10dp",
        right: "10dp",
        height: Titanium.UI.SIZE,
        layout: "horizontal",
        id: "__alloyId161"
    });
    $.__views.__alloyId153.add($.__views.__alloyId161);
    $.__views.__alloyId162 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        width: "50%",
        id: "__alloyId162"
    });
    $.__views.__alloyId161.add($.__views.__alloyId162);
    $.__views.__alloyId163 = Ti.UI.createLabel({
        text: "CHANGE",
        left: "0",
        right: "5",
        backgroundColor: "#D42220",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
        height: "40dp",
        id: "__alloyId163"
    });
    $.__views.__alloyId162.add($.__views.__alloyId163);
    doChange ? $.__views.__alloyId163.addEventListener("click", doChange) : __defers["$.__views.__alloyId163!click!doChange"] = true;
    $.__views.__alloyId164 = Ti.UI.createView({
        layout: "vertical",
        height: Titanium.UI.SIZE,
        width: "50%",
        id: "__alloyId164"
    });
    $.__views.__alloyId161.add($.__views.__alloyId164);
    $.__views.__alloyId165 = Ti.UI.createLabel({
        text: "ACCEPT",
        left: "5",
        right: "0",
        backgroundColor: "#D42220",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
        height: "40dp",
        id: "__alloyId165"
    });
    $.__views.__alloyId164.add($.__views.__alloyId165);
    sendDetails ? $.__views.__alloyId165.addEventListener("click", sendDetails) : __defers["$.__views.__alloyId165!click!sendDetails"] = true;
    $.__views.disableViewBgLoadingIndicator = Ti.UI.createView({
        visible: "false",
        id: "disableViewBgLoadingIndicator",
        top: "0",
        zIndex: "20",
        height: "100%",
        width: "100%",
        bottom: "0",
        backgroundColor: "black",
        opacity: "0.7"
    });
    $.__views.reviewRedeemForm.add($.__views.disableViewBgLoadingIndicator);
    $.__views.activityIndicator = Ti.UI.createActivityIndicator({
        zIndex: "21",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        id: "activityIndicator"
    });
    $.__views.reviewRedeemForm.add($.__views.activityIndicator);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var indicator = false;
    $.nameL.setText("");
    $.surnameL.setText("");
    $.streetL.setText("");
    $.streetNL.setText("");
    $.cityL.setText("");
    $.postCL.setText("");
    $.mobL.setText("");
    var reviewRedeemFormData = {};
    var indicator = false;
    Ti.App.addEventListener("view:reviewRedeemForm", function(e) {
        Ti.API.info("fired event view:reviewRedeemForm ------Captured");
        reviewRedeemFormData = e;
        toggleErrorMsg("");
        $.nameL.setText(e.nameField);
        $.surnameL.setText(e.surnameField);
        $.streetL.setText(e.addressField);
        $.streetNL.setText(e.streetNField);
        $.cityL.setText(e.cityField);
        $.postCL.setText(e.postCodeField);
        $.mobL.setText(e.mobileNField);
    });
    __defers["$.__views.__alloyId163!click!doChange"] && $.__views.__alloyId163.addEventListener("click", doChange);
    __defers["$.__views.__alloyId165!click!sendDetails"] && $.__views.__alloyId165.addEventListener("click", sendDetails);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;