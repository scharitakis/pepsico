function Controller() {
    function connectionNotification(online) {
        Alloy.Globals.online = online;
        if (online) {
            $.ds.contentview.setTop("44dp");
            $.ds.internetNotification.setVisible(false);
        } else {
            $.ds.contentview.setTop("64dp");
            $.ds.internetNotification.setVisible(true);
        }
    }
    function rowSelect(e) {
        var views = $.ds.contentview.getViews();
        var currentView = views[$.ds.contentview.getCurrentPage()];
        Alloy.Globals.previousView = {
            row: {
                name: $.ds.navTitle.getText(),
                viewsrc: currentView.id,
                scroll: false
            }
        };
        if (currentView.id != e.row.viewsrc) {
            var num = _.map(views, function(num) {
                return num.id;
            }).indexOf(e.row.viewsrc);
            if (-1 == num) {
                Ti.API.info("new View");
                var newView = Alloy.createController(e.row.viewsrc).getView();
                $.ds.contentview.addView(newView);
                Ti.API.info(JSON.stringify(newView));
                setTimeout(function() {
                    e.row.scroll ? $.ds.contentview.scrollToView(newView) : $.ds.contentview.setCurrentPage(views.length);
                }, 500);
            } else {
                Ti.API.info("num");
                e.row.scroll ? $.ds.contentview.scrollToView(views[num]) : $.ds.contentview.setCurrentPage(num);
            }
            "home" == e.row.viewsrc ? $.ds.navTitle.setText("") : $.ds.navTitle.setText(e.row.name);
            Ti.App.fireEvent("view:" + e.row.viewsrc, e.row.props);
        }
    }
    function preloadViews() {
        var views = $.ds.contentview.getViews();
        for (var i = 0; preloadViewsList.length > i; i++) {
            var num = _.map(views, function(num) {
                return num.id;
            }).indexOf(preloadViewsList[i]);
            if (-1 == num) {
                var newView = Alloy.createController(preloadViewsList[i]).getView();
                $.ds.contentview.addView(newView);
            }
        }
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.win = Ti.UI.createWindow({
        backgroundColor: "#00163b",
        id: "win",
        navBarHidden: "true",
        exitOnClose: "true"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.ds = Alloy.createWidget("ds.slideMenu", "widget", {
        id: "ds",
        __parentSymbol: $.__views.win
    });
    $.__views.ds.setParent($.__views.win);
    exports.destroy = function() {};
    _.extend($, $.__views);
    connectionNotification(Titanium.Network.online);
    Titanium.Network.addEventListener("change", function(e) {
        connectionNotification(e.online);
    });
    Alloy.Globals.setupUI = function(d) {
        var leftData = [];
        var rightData = [];
        if (d) {
            var leftMenuData = [ {
                name: "HOME",
                viewsrc: "home",
                image: null,
                props: null,
                scroll: false
            }, {
                name: "Prizes",
                viewsrc: "prizes",
                image: null,
                props: null,
                scroll: false
            }, {
                name: "FAQ",
                viewsrc: "faq",
                image: null,
                props: null,
                scroll: false
            }, {
                name: "Login",
                viewsrc: "login",
                image: null,
                props: null,
                scroll: false
            } ];
            var rightMenuData = [ {
                name: "HOME",
                viewsrc: "home",
                image: null,
                props: null,
                scroll: false
            }, {
                name: "HOW TO PLAY",
                viewsrc: "howtoplay",
                image: null,
                props: null,
                scroll: false
            }, {
                name: "FAQ",
                viewsrc: "faq",
                image: null,
                props: null,
                scroll: false
            } ];
        } else {
            var leftMenuData = [ {
                name: "Profile",
                viewsrc: "profile",
                image: null,
                props: null,
                scroll: false
            }, {
                name: "HOME",
                viewsrc: "home",
                image: null,
                props: null,
                scroll: false
            }, {
                name: "Prizes",
                viewsrc: "prizes",
                image: null,
                props: null,
                scroll: false
            }, {
                name: "FAQ",
                viewsrc: "faq",
                image: null,
                props: null,
                scroll: false
            }, {
                name: "Logout",
                viewsrc: "logout",
                image: null,
                props: null,
                scroll: false
            } ];
            var rightMenuData = [ {
                name: "Profile",
                viewsrc: "profile",
                image: null,
                props: null,
                scroll: false
            }, {
                name: "HOME",
                viewsrc: "home",
                image: null,
                props: null,
                scroll: false
            }, {
                name: "Activities",
                viewsrc: "activities",
                image: null,
                props: null,
                scroll: false
            }, {
                name: "Prizes",
                viewsrc: "prizes",
                image: null,
                props: null,
                scroll: false
            }, {
                name: "FAQ",
                viewsrc: "faq",
                image: null,
                props: null,
                scroll: false
            }, {
                name: "Logout",
                viewsrc: "logout",
                image: null,
                props: null,
                scroll: false
            }, {
                name: "Settings",
                viewsrc: "settings",
                image: null,
                props: null,
                scroll: false
            } ];
        }
        for (var i = 0; leftMenuData.length > i; i++) {
            var rowl = Alloy.createController("leftMenuRow", leftMenuData[i]).getView();
            leftData.push(rowl);
        }
        for (var i = 0; rightMenuData.length > i; i++) {
            var rowr = Alloy.createController("rightMenuRow", rightMenuData[i]).getView();
            rightData.push(rowr);
        }
        "iphone" === Ti.Platform.osname && ($.ds.leftTableView.separatorStyle = Ti.UI.iPhone.TableViewSeparatorStyle.NONE);
        $.ds.leftTableView.removeAllChildren();
        $.ds.rightTableView.removeAllChildren();
        $.ds.leftTableView.data = leftData;
        $.ds.rightTableView.data = rightData;
        if (d) {
            var firstView = Alloy.createController("home").getView();
            $.ds.contentview.addView(firstView);
            $.ds.contentview.setCurrentPage(0);
            $.ds.navTitle.setText("");
        }
    };
    Alloy.Globals.setupUI(true);
    Ti.App.addEventListener("changeView", function(e) {
        rowSelect(e);
    });
    $.ds.leftTableView.addEventListener("click", function(e) {
        if ("" != e.row.viewsrc) {
            rowSelect(e);
            $.ds.toggleLeftSlider();
        }
    });
    $.ds.rightTableView.addEventListener("click", function(e) {
        rowSelect(e);
        $.ds.toggleRightSlider();
    });
    $.ds.leftTableView.addEventListener("touchstart", function() {});
    $.ds.leftTableView.addEventListener("touchend", function() {});
    $.ds.leftTableView.addEventListener("scroll", function() {});
    $.ds.rightTableView.addEventListener("touchstart", function() {});
    $.ds.rightTableView.addEventListener("touchend", function() {});
    $.ds.rightTableView.addEventListener("scroll", function() {});
    Ti.App.addEventListener("sliderToggled", function(e) {
        if ("right" == e.direction) {
            $.ds.leftMenu.zIndex = 2;
            $.ds.rightMenu.zIndex = 1;
        } else if ("left" == e.direction) {
            $.ds.leftMenu.zIndex = 1;
            $.ds.rightMenu.zIndex = 2;
        }
    });
    var UrbanAirship = require("ti.urbanairship");
    UrbanAirship.tags = [ "testingtesting", "appcelerator", "pepsiCo" ];
    UrbanAirship.alias = "pepsiCo";
    if (Alloy.Globals.online) {
        Alloy.Globals.isiOS = true;
        UrbanAirship.autoBadge = true;
        UrbanAirship.autoResetBadge = true;
        Ti.Network.registerForPushNotifications({
            types: [ Ti.Network.NOTIFICATION_TYPE_BADGE, Ti.Network.NOTIFICATION_TYPE_ALERT, Ti.Network.NOTIFICATION_TYPE_SOUND ],
            success: function(e) {
                var token = e.deviceToken;
                UrbanAirship.registerDevice(token);
                Alloy.Globals.PushToken = token;
            },
            error: function() {},
            callback: function(e) {
                UrbanAirship.handleNotification(e.data);
                alert(e.data);
            }
        });
    }
    var preloadViewsList = [ "coupon", "winnerView", "nowinnerView", "pinDialog", "redeemForm", "reviewRedeemForm", "redeemSuccess", "howtoplay", "faq" ];
    if ("iphone" === Ti.Platform.osname) {
        $.win.statusBarStyle = Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT;
        $.win.open({
            transition: Titanium.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT
        });
    } else {
        $.win.setOrientationModes([ Titanium.UI.PORTRAIT ]);
        $.win.open();
    }
    $.win.addEventListener("open", function() {
        preloadViews();
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;