function WPATH(s) {
    var index = s.lastIndexOf("/");
    var path = -1 === index ? "ds.slideMenu/" + s : s.substring(0, index) + "/ds.slideMenu/" + s.substring(index + 1);
    return path;
}

module.exports = [ {
    isId: true,
    priority: 100000.0002,
    key: "leftMenu",
    style: {
        top: "0dp",
        left: "0dp",
        width: "250dp",
        zIndex: "2",
        backgroundColor: "#FFF"
    }
}, {
    isId: true,
    priority: 100000.0003,
    key: "rightMenu",
    style: {
        top: "0dp",
        right: "0dp",
        width: "250dp",
        zIndex: "1",
        backgroundColor: "#FFF"
    }
}, {
    isId: true,
    priority: 100000.0004,
    key: "navview",
    style: {
        top: "0dp",
        left: "0dp",
        width: Titanium.UI.FILL,
        height: "44dp",
        backgroundColor: "#00225d"
    }
}, {
    isId: true,
    priority: 100000.0005,
    key: "movableview",
    style: {
        left: "0",
        zIndex: "3",
        width: Titanium.UI.FILL
    }
}, {
    isId: true,
    priority: 100000.0006,
    key: "contentview",
    style: {
        left: "0dp",
        width: Titanium.UI.FILL,
        height: Ti.UI.Fill,
        top: "44",
        backgroundColor: "white"
    }
}, {
    isId: true,
    priority: 100000.0007,
    key: "shadowview",
    style: {
        shadowColor: "black",
        shadowOffset: {
            x: "0",
            y: "0"
        },
        shadowRadius: "2.5"
    }
}, {
    isId: true,
    priority: 100000.0008,
    key: "leftButton",
    style: {
        image: "/ds.slideMenu/ButtonMenu.png",
        left: "10",
        top: "10",
        width: "22",
        height: "22"
    }
}, {
    isId: true,
    priority: 100000.0009,
    key: "logo",
    style: {
        image: "/ds.slideMenu/pepsi-logo.png",
        left: "2",
        top: "3",
        width: "90dp"
    }
}, {
    isId: true,
    priority: 100000.001,
    key: "rightButton",
    style: {
        right: "0",
        top: "0",
        width: "44",
        height: "44"
    }
}, {
    isId: true,
    priority: 100000.0011,
    key: "rightButtonImage",
    style: {
        image: "/ds.slideMenu/ButtonMenu.png",
        right: "10",
        top: "10",
        width: "22",
        height: "22"
    }
}, {
    isId: true,
    priority: 100000.0012,
    key: "navTitle",
    style: {
        font: {
            fontFamily: "FuturaStd-Bold",
            fontSize: "16dp",
            fontWeight: "bold"
        },
        color: "white"
    }
}, {
    isId: true,
    priority: 100000.0013,
    key: "rightTableView",
    style: {
        backgroundColor: "#00225d"
    }
}, {
    isId: true,
    priority: 100000.0014,
    key: "leftTableView",
    style: {
        backgroundColor: "#00225d"
    }
} ];