var Alloy = require("alloy"), _ = Alloy._, Backbone = Alloy.Backbone;

Alloy.Globals.online = Titanium.Network.online;

Alloy.Globals.userMsisdn = "";

Ti.App.Properties.hasProperty("user") && (Alloy.Globals.userMsisdn = Ti.App.Properties.getObject("user")["msisdn"]);

Alloy.Globals.serverUrl = "http://10.1.3.40:8050/promo/web/";

Alloy.Globals.doPost = function(data, url, loadCallBack, errCallBack) {
    var xhr = Titanium.Network.createHTTPClient();
    xhr.onload = loadCallBack;
    xhr.onerror = errCallBack;
    xhr.open("POST", url);
    xhr.send(data);
};

Alloy.Globals.doGET = function(url, loadCallBack, errCallBack) {
    var xhr = Titanium.Network.createHTTPClient();
    xhr.onload = loadCallBack;
    xhr.onerror = errCallBack;
    xhr.open("GET", url);
    xhr.send();
};

Alloy.createController("index");