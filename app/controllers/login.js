Ti.App.addEventListener("view:login",function(e){
	Ti.API.info("fired event view:login ------Captured");
	Ti.API.info(e);
});

function test(){
	//alert("this is a test");
	Alloy.Globals.setupUI(false);
}






function test1() {

	/*=======GOOGLE LOGIN =======*/
	function doClick(e) {
		var Google = require('GoogleService').GoogleService;
		//Make sure path is correct

		// Google params
		var ggParams = {
			clientId : '358496948112-kh0hrbjp31igr6rhokd12kp2g52qfun8.apps.googleusercontent.com',
			clientSecret : '66GqySGncQimfszgknjpfyEx',
			redirectUri : 'urn:ietf:wg:oauth:2.0:oob',
			devKey : '<replace with dev key>',
		};

		// Initialize Google Service
		var google = new Google(ggParams);

		google.login(function(e) {
			Ti.API.info('Token: ' + google.accessToken());

			google.refreshToken(function(e) {
				Ti.API.info('New Token: ' + e.token);
			});

			var params = {
				params : [],
				call : 'userinfo',
				method : 'GET'
			};

			google.callMethod(params, function(e) {
				Ti.API.info(e);
			}, null);
		});

	}

	/*=======GOOGLE LOGOUT =======*/

	var logoutGooglebtn = Ti.UI.createButton({
		title : 'logout Google',
		width : 70,
		height : 40,
		left : 0,
		top : 310,
	});

	logoutGooglebtn.addEventListener('click', function() {
		Ti.App.Properties.removeProperty("GG_ACCESS_TOKEN");
		Ti.Network.createHTTPClient().clearCookies('https://accounts.google.com/o/oauth2/');

	});
	$.index.add(logoutGooglebtn);

	/*=======++++++++++++++=======*/

	/*-----Facebook login -------------

	 *
	 *
	 *
	 *
	 * */
	var fb = require('facebook');
	fb.appid = "213868488820229";
	fb.permissions = ['publish_stream', 'publish_actions', 'read_friendlists'];
	fb.forceDialogAuth = true;
	fb.addEventListener('login', function(e) {
		if (e.success) {
			var ss = fb.getAccessToken();
			//alert(ss);
			alert('Logged in');
		}
	});

	fb.addEventListener('logout', function(e) {
		Ti.Network.createHTTPClient().clearCookies('https://m.facebook.com');
		Ti.Network.createHTTPClient().clearCookies('http://m.facebook.com');
		//alert('Logged out');
	});

	$.index.add(fb.createLoginButton({
		top : 50,
		style : fb.BUTTON_STYLE_WIDE
	}));

	// logout BTN
	var logoutFBbtn = Ti.UI.createButton({
		title : 'logout',
		width : 70,
		height : 40,
		left : 0,
		top : 110,
	});

	logoutFBbtn.addEventListener('click', function() {
		fb.logout();
	});
	$.index.add(logoutFBbtn);

	// logout BTN
	var propertiesBtn = Ti.UI.createButton({
		title : 'getProperties',
		width : 70,
		height : 40,
		left : 70,
		top : 110,
	});

	propertiesBtn.addEventListener('click', function() {
		var properties = Ti.App.Properties.listProperties();
		var s = "";
		for (var i = 0, ilen = properties.length; i < ilen; i++) {
			var value = Ti.App.Properties.getString(properties[i]);
			Ti.API.info(properties[i] + ' = ' + value);
			s = s + "-------" + properties[i] + ' = ' + value;
		}
		//alert(s);
		var aa = Ti.App.Properties.getString("FBAccessToken");
		//alert(aa);

	});
	$.index.add(propertiesBtn);

	//alert(Ti.App.Properties.listProperties());

	// Get user info
	var getOnFBbtn = Ti.UI.createButton({
		title : 'FB GET',
		width : 70,
		height : 40,
		left : 0,
		top : 80,
	});

	getOnFBbtn.addEventListener('click', function() {

		fb.requestWithGraphPath('me', {}, 'GET', function(e) {
			if (e.success) {
				//alert(e.result);
			} else if (e.error) {
				//alert(e.error);
			} else {
				//alert('Unknown response');
			}
		});
	});
	$.index.add(getOnFBbtn);

	// Post on wall
	var postOnFBbtn = Ti.UI.createButton({
		title : 'FB Post',
		width : 70,
		height : 40,
		left : 70,
		top : 80,
	});

	postOnFBbtn.addEventListener('click', function() {
		var data = {
			"message" : "This is a test message"
		};
		fb.requestWithGraphPath('me/feed', data, 'POST', function(e) {
			if (e.success) {
				//alert(e.result);
			} else if (e.error) {
				//alert(e.error);
			} else {
				//alert('Unknown response');
			}
		});
	});
	$.index.add(postOnFBbtn);

	// Invite
	var inviteOnFBbtn = Ti.UI.createButton({
		title : 'FB Invite',
		width : 70,
		height : 40,
		left : 140,
		top : 80,
	});

	inviteOnFBbtn.addEventListener('click', function() {
		// if (!fb.loggedIn){
		// 	fb.authorize();
		//}else{
		fb.dialog('apprequests', {
			message : 'my custom message'
		}, function(data) {
			Ti.API.info(data);
		});
		//}

	});

	$.index.add(inviteOnFBbtn);

	// Post on wall Dialog
	var postOnFBDbtn = Ti.UI.createButton({
		title : 'FB Post Dialog',
		width : 70,
		height : 40,
		left : 210,
		top : 80,
	});

	postOnFBDbtn.addEventListener('click', function() {
		var data = {
			link : "http://www.appcelerator.com",
			name : "Appcelerator Titanium Mobile",
			message : "Checkout this cool open source project for creating mobile apps",
			caption : "Appcelerator Titanium Mobile",
			picture : "http://developer.appcelerator.com/assets/img/DEV_titmobile_image.png",
			description : "You've got the ideas, now you've got the power. Titanium translates " + "your hard won web skills into native applications..."

		};
		//if (!fb.loggedIn){
		// 	fb.authorize();
		//}else{
		fb.dialog('feed', data, function(e) {
			if (e.success && e.result) {
				//alert("Success! New Post ID: " + e.result);
			} else {
				if (e.error) {
					//alert(e.error);
				} else {
					//alert("User canceled dialog.");
				}
			}
		});

		//}
	});
	$.index.add(postOnFBDbtn);

	/*-----Facebook login END */

	/*Push Notifications*/

	var btnOpen = Ti.UI.createButton({
		title : 'Open UA Inbox',
		width : 200,
		height : 40,
		top : 140,
	});
	btnOpen.addEventListener('click', function() {
		// Open default mailbox
		UrbanAirship.displayInbox({
			animated : true
		});
	});
	$.index.add(btnOpen);

	$.index.add(Ti.UI.createLabel({
		text : 'DeviceID:',
		top : 180,
		left : 10,
		textAlign : 'left',
		color : 'black',
		font : {
			fontSize : 18,
			fontStyle : 'bold'
		},
		height : Ti.UI.SIZE || 'auto'
	}));

	var labelID = Ti.UI.createLabel({
		text : '',
		top : 10,
		left : 10,
		right : 10,
		textAlign : 'left',
		color : 'black',
		height : Ti.UI.SIZE || 'auto'
	});
	$.index.add(labelID);

	$.index.add(Ti.UI.createLabel({
		text : 'Last Message:',
		top : 10,
		left : 10,
		textAlign : 'left',
		color : 'black',
		font : {
			fontSize : 18,
			fontStyle : 'bold'
		},
		height : Ti.UI.SIZE || 'auto'
	}));
	var labelMessage = Ti.UI.createLabel({
		text : '',
		top : 10,
		left : 10,
		right : 10,
		textAlign : 'left',
		color : 'black',
		height : Ti.UI.SIZE || 'auto'
	});
	$.index.add(labelMessage);

	$.index.add(Ti.UI.createLabel({
		text : 'Payload:',
		top : 10,
		left : 10,
		textAlign : 'left',
		color : 'black',
		font : {
			fontSize : 18,
			fontStyle : 'bold'
		},
		height : Ti.UI.SIZE || 'auto'
	}));

	var labelPayload = Ti.UI.createLabel({
		text : ' ',
		top : 10,
		left : 10,
		right : 10,
		textAlign : 'left',
		color : 'black',
		height : Ti.UI.SIZE || 'auto'
	});
	$.index.add(labelPayload);

	/*  Twitter Social */
	var social = require('alloy/social').create({
		consumerSecret : '67AZOIObqutgn2fhwUziTCq3OiVf2cPIl4WixZzCrs',
		consumerKey : 'H8FksViCedufGTCoR7tyMw'
	});

	var TwitterBtn = Ti.UI.createButton({
		title : 'Twitter btn ',
		width : 200,
		height : 40,
		top : 300,
	});

	TwitterBtn.addEventListener('click', function() {
		// Open default mailbox

		// If not authorized, get authorization from the user
		if (!social.isAuthorized()) {
			social.authorize();
		} else {

			// Post a message
			// Setup both callbacks for confirmation
			social.share({
				message : "this is stavros",
				success : function(e) {
					//alert('Success!');
				},
				error : function(e) {
					//alert(e);
				}
			});
		}

	});
	$.index.add(TwitterBtn);
	/*  Twitter Social END */

	$.index.open();

	if (Ti.Platform.name == "iPhone OS") {
		alert(1);
		var UrbanAirship = require('ti.urbanairship');
		Ti.API.info("module is => " + UrbanAirship);

		/*
		* Urban Airship will load the options from an AirshipConfig.plist file that
		* should be stored in the application bundle. You will find an example
		* AirshipConfig.plist file in the 'example/platform/iphone' folder of the module.
		*/

		// Set UA options
		UrbanAirship.tags = ['testingtesting', 'appcelerator', 'pepsiCo'];
		UrbanAirship.alias = 'pepsiCo';
		UrbanAirship.autoBadge = true;
		UrbanAirship.autoResetBadge = true;

		function eventCallback(e) {
			// Pass the notification to the module
			UrbanAirship.handleNotification(e.data);

			//Ti.API.info('Push message received');
			//Ti.API.info('  Message: ' + e.data.alert);
			//Ti.API.info('  Payload: ' + e.data.aps);
			//alert(e.data.alert);
			labelMessage.text = e.data.alert;
			labelPayload.text = JSON.stringify(e.data.aps);
		}

		function eventSuccess(e) {
			// *MUST* pass the received token to the module
			UrbanAirship.registerDevice(e.deviceToken);

			Ti.API.info('Received device token: ' + e.deviceToken);
			labelID.text = e.deviceToken;
			btnOpen.enabled = true;
		}

		function eventError(e) {
			Ti.API.info('Error:' + e.error);
			var alert = Ti.UI.createAlertDialog({
				title : 'Error',
				message : e.error
			});
			alert.show();
		}


		Ti.Network.registerForPushNotifications({
			types : [Ti.Network.NOTIFICATION_TYPE_BADGE, Ti.Network.NOTIFICATION_TYPE_ALERT, Ti.Network.NOTIFICATION_TYPE_SOUND],
			success : eventSuccess,
			error : eventError,
			callback : eventCallback
		});

	} else {
		//alert(2);
		var UrbanAirship = require('ti.urbanairship');
		Ti.API.info("module is => " + UrbanAirship);

		// Set UA options
		UrbanAirship.showOnAppClick = true;
		UrbanAirship.tags = ['pepsiCo', 'appcelerator', 'my-tags'];
		UrbanAirship.alias = 'pepsiCo';

		// Display current pushId (use ua.c2dmId if using C2DM)
		//labelID.text = UrbanAirship.pushId;

		// Set switch to current state of push
		//pushOnSwitch.value = UrbanAirship.pushEnabled;

		// Toggle push state on switch change
		//pushOnSwitch.addEventListener('change', function (e) {
		//	UrbanAirship.pushEnabled = e.value;
		//});

		UrbanAirship.pushEnabled = true;

		function eventCallback(e) {
			if (e.clicked) {
				Ti.API.info('User clicked a notification');
			} else {
				Ti.API.info('Push message received');
			}
			Ti.API.info('  Message: ' + e.message);
			Ti.API.info('  Payload: ' + e.payload);

			//labelMessage.text = e.message;
			//labelPayload.text = e.payload;
		}

		function eventSuccess(e) {
			Ti.API.info('Received device token: ' + e.deviceToken);
			//labelAPID.text = e.deviceToken;
		}

		function eventError(e) {
			Ti.API.info('Error:' + e.error);
			var alert = Ti.UI.createAlertDialog({
				title : 'Error',
				message : e.error
			});
			alert.show();
		}


		UrbanAirship.addEventListener(UrbanAirship.EVENT_URBAN_AIRSHIP_CALLBACK, eventCallback);
		UrbanAirship.addEventListener(UrbanAirship.EVENT_URBAN_AIRSHIP_SUCCESS, eventSuccess);
		UrbanAirship.addEventListener(UrbanAirship.EVENT_URBAN_AIRSHIP_ERROR, eventError);

	};

}
