/*=======Internet Connection Notification =======
 =======================================*/

function connectionNotification(online){
	Alloy.Globals.online = online;
	if(!online){
		$.ds.contentview.setTop("64dp");
		$.ds.internetNotification.setVisible(true);
	 }else{	 	
	 	$.ds.contentview.setTop("44dp");
	 	$.ds.internetNotification.setVisible(false);
	 }
};

connectionNotification(Titanium.Network.online);


Titanium.Network.addEventListener('change', function(e)
{
	connectionNotification(e.online);
	
});
/*=======Internet Connection Notification  END =======
 =======================================*/


var leftData = [];
var rightData = [];


// Pass data to widget leftTableView and rightTableView
Alloy.Globals.setupUI = function(d){
	var leftData = [];
	var rightData = [];
	
	
	if(d){
	var leftMenuData=[{name:"HOME",viewsrc:"home",image:null,props:null,scroll:false},
					  {name:"Prizes",viewsrc:"prizes",image:null,props:null,scroll:false},
					  {name:"FAQ",viewsrc:"faq",image:null,props:null,scroll:false},
					  {name:"Login",viewsrc:"login",image:null,props:null,scroll:false},
					  ];
					  
   var rightMenuData=[{name:"HOME",viewsrc:"home",image:null,props:null,scroll:false},
					  {name:"HOW TO PLAY",viewsrc:"howtoplay",image:null,props:null,scroll:false},
					  {name:"FAQ",viewsrc:"faq",image:null,props:null,scroll:false}
					  ];
	}else{
		var leftMenuData=[
					  {name:"Profile",viewsrc:"profile",image:null,props:null,scroll:false},
					  {name:"HOME",viewsrc:"home",image:null,props:null,scroll:false},
					  {name:"Prizes",viewsrc:"prizes",image:null,props:null,scroll:false},
					  {name:"FAQ",viewsrc:"faq",image:null,props:null,scroll:false},
					  {name:"Logout",viewsrc:"logout",image:null,props:null,scroll:false},
					  ];
					  
   		var rightMenuData=[
   						{name:"Profile",viewsrc:"profile",image:null,props:null,scroll:false},
   					 	{name:"HOME",viewsrc:"home",image:null,props:null,scroll:false},
   					 	{name:"Activities",viewsrc:"activities",image:null,props:null,scroll:false},
					 	{name:"Prizes",viewsrc:"prizes",image:null,props:null,scroll:false},
					 	{name:"FAQ",viewsrc:"faq",image:null,props:null,scroll:false},					  
					  	{name:"Logout",viewsrc:"logout",image:null,props:null,scroll:false},
					  	{name:"Settings",viewsrc:"settings",image:null,props:null,scroll:false}	
					  	];
	}
	
	for(var i=0;i<leftMenuData.length;i++)
	{
		var rowl = Alloy.createController('leftMenuRow', leftMenuData[i]).getView();
		leftData.push(rowl);
	}
	
	for(var i=0;i<rightMenuData.length;i++)
	{
		var rowr = Alloy.createController('rightMenuRow', rightMenuData[i]).getView();
		rightData.push(rowr);
	}
	
	if (Ti.Platform.osname === 'iphone'){
		$.ds.leftTableView.separatorStyle = Ti.UI.iPhone.TableViewSeparatorStyle.NONE; //remove the separator lines
	}
	
	//remove children first 
	$.ds.leftTableView.removeAllChildren();
	$.ds.rightTableView.removeAllChildren(); 
	
	//add the rows
	$.ds.leftTableView.data = leftData;
	$.ds.rightTableView.data = rightData;
	
	if(d){
		var firstView = Alloy.createController("home").getView();
		$.ds.contentview.addView(firstView);
		$.ds.contentview.setCurrentPage(0);
		$.ds.navTitle.setText("");
		
	}
	
};


//TODO: if user is logged in then we need to pass false
Alloy.Globals.setupUI(true);





Ti.App.addEventListener("changeView", function(e) {
	//setTimeout(function(){
    	rowSelect(e);
	//}, 100);
	
});



function rowSelect(e) {
	var views = $.ds.contentview.getViews();
	var currentView = views[$.ds.contentview.getCurrentPage()];
	Alloy.Globals.previousView = {row:{name:$.ds.navTitle.getText(),viewsrc:currentView.id,scroll:false}};
	if (currentView.id != e.row.viewsrc) {
		var num = _.map(views, function(num, key){ return num.id; }).indexOf(e.row.viewsrc);
		if(num==-1)
		{
			Ti.API.info("new View");
			var newView = Alloy.createController(e.row.viewsrc).getView();
			$.ds.contentview.addView(newView);
			Ti.API.info(JSON.stringify(newView));
			setTimeout(function(){
    			if(e.row.scroll){
					$.ds.contentview.scrollToView(newView);
				}else{
					$.ds.contentview.setCurrentPage(views.length);
				}
			}, 500);
		}else
		{
			Ti.API.info("num");
			//alert(views[num].id);
			if(e.row.scroll){
				$.ds.contentview.scrollToView(views[num]);
			}else{
				$.ds.contentview.setCurrentPage(num);
			}
			
		}
		if(e.row.viewsrc=="home"){
			$.ds.navTitle.setText("");
		}else{
			$.ds.navTitle.setText(e.row.name);
		}
		Ti.App.fireEvent("view:"+e.row.viewsrc, e.row.props);
	}
	
}

//$.ds.contentview.addEventListener("scrollend",function(e){alert("scrollend");});




// Swap views on menu item click
$.ds.leftTableView.addEventListener('click', function selectRow(e) {
	if(e.row.viewsrc!=""){
		rowSelect(e);
		if (Ti.Platform.name != "iPhone OS"){
			setTimeout(function(){
    			$.ds.toggleLeftSlider();
			}, 500);
		}else{
			$.ds.toggleLeftSlider();
		}
		
	}
});
$.ds.rightTableView.addEventListener('click', function selectRow(e) {
	rowSelect(e);
	if (Ti.Platform.name != "iPhone OS"){
			setTimeout(function(){
    			$.ds.toggleRightSlider();
			}, 500);
	}else{
			$.ds.toggleRightSlider();
	}	
	
});

// Set row title highlight colour (left table view)
var storedRowTitle = null;
$.ds.leftTableView.addEventListener('touchstart', function(e) {
	//storedRowTitle = e.row.customTitle;
	//storedRowTitle.color = "#FFF";
});
$.ds.leftTableView.addEventListener('touchend', function(e) {
	//storedRowTitle.color = "#666";
});
$.ds.leftTableView.addEventListener('scroll', function(e) {
	//if (storedRowTitle != null)
	//	storedRowTitle.color = "#666";
});

// Set row title highlight colour (right table view)
var storedRowTitle = null;
$.ds.rightTableView.addEventListener('touchstart', function(e) {
	//storedRowTitle = e.row.customTitle;
	//storedRowTitle.color = "#FFF";
});
$.ds.rightTableView.addEventListener('touchend', function(e) {
	//storedRowTitle.color = "#666";
});
$.ds.rightTableView.addEventListener('scroll', function(e) {
	//if (storedRowTitle != null)
	//	storedRowTitle.color = "#666";
});

Ti.App.addEventListener("sliderToggled", function(e) {
	
	if (e.direction == "right") {
		$.ds.leftMenu.zIndex = 2;
		$.ds.rightMenu.zIndex = 1;
	} else if (e.direction == "left") {
		$.ds.leftMenu.zIndex = 1;
		$.ds.rightMenu.zIndex = 2;
	}
});

/*=======PUSH Notifications=======
 * 
 * 
 * 
 * */

var UrbanAirship = require('ti.urbanairship');
UrbanAirship.tags = [ 'testingtesting', 'appcelerator', 'pepsiCo' ];
UrbanAirship.alias = 'pepsiCo';
if(Alloy.Globals.online){
	if (Ti.Platform.name == "iPhone OS"){
		Alloy.Globals.isiOS = true; 
		UrbanAirship.autoBadge = true;
		UrbanAirship.autoResetBadge = true;
		
		Ti.Network.registerForPushNotifications({
		    types:[
		        Ti.Network.NOTIFICATION_TYPE_BADGE,
		        Ti.Network.NOTIFICATION_TYPE_ALERT,
		        Ti.Network.NOTIFICATION_TYPE_SOUND
		    ],
		    success: function(e) {
		        var token = e.deviceToken;
		        UrbanAirship.registerDevice(token);
				//alert("Your token is: "+token); 
				Alloy.Globals.PushToken = token;
				   
		    },
		    error: function(e) {
		        //alert("Error: " + e.error);
		    },
		    callback: function(e) {
		        UrbanAirship.handleNotification(e.data);
		        alert(e.data);
		    }
		});
	}else{
		UrbanAirship.showOnAppClick = true;
		UrbanAirship.pushEnabled = true;
		Alloy.Globals.isiOS = false; 
		//alert('Urban Push id ='+UrbanAirship.pushId);
		
		function eventCallback(e) {
			if (e.clicked) {
				Ti.API.info('User clicked a notification');
			} else {
				Ti.API.info('Push message received');
			}
			Ti.API.info('  Message: ' + e.message);
			Ti.API.info('  Payload: ' + e.payload);
		
			//alert('Callback='+e);
		}
		
		function eventSuccess(e) {
			Ti.API.info('Received device token: ' + e.deviceToken);
			//alert('Device token='+e.deviceToken);
			Alloy.Globals.PushToken = e.deviceToken;
		}
		
		function eventError(e) {
			Ti.API.info('Error:' + e.error);
			var alert = Ti.UI.createAlertDialog({
				title:'Error',
				message:e.error
			});
			alert.show();
		}
		
		UrbanAirship.addEventListener(UrbanAirship.EVENT_URBAN_AIRSHIP_CALLBACK, eventCallback);
		UrbanAirship.addEventListener(UrbanAirship.EVENT_URBAN_AIRSHIP_SUCCESS, eventSuccess);
		UrbanAirship.addEventListener(UrbanAirship.EVENT_URBAN_AIRSHIP_ERROR, eventError);
	}
}

/*=======PUSH Notifications END=======
 =======================================*/


var preloadViewsList = ["coupon",
						"winnerView",
						"nowinnerView",
						"pinDialog",
						"redeemForm",
						"reviewRedeemForm",
						"redeemSuccess",
						"howtoplay",
						"faq"];
	
function preloadViews(){
	var views = $.ds.contentview.getViews();
	for(var i=0;i<preloadViewsList.length; i++){
		var num = _.map(views, function(num, key){ return num.id; }).indexOf(preloadViewsList[i]);
		if(num==-1){
			var newView = Alloy.createController(preloadViewsList[i]).getView();
			$.ds.contentview.addView(newView);
		}
	}
}

//preloadViews();



if (Ti.Platform.osname === 'iphone'){
	//change the status bar color( hour , battery etc..)
	$.win.statusBarStyle = Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT;
	$.win.open({
		transition : Titanium.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT
	});
}
else{
	$.win.setOrientationModes([Titanium.UI.PORTRAIT]); 
	$.win.open();
}


$.win.addEventListener("open",function(e){
	preloadViews();
});
