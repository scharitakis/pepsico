/*Ti.App.addEventListener("sliderToggled", function(e) {
if (e.hasSlided) {
//	$.table.touchEnabled = false;
}
else {
//	$.table.touchEnabled = true;
}
});*/

// This will be triggered when

var RedeemFormData={};


function toggleErrorMsg(msg){
	if(msg!="")
	{
		//Show error
		$.errorMsgView.setVisible(true);
		$.errorMsgView.setHeight(Titanium.UI.SIZE);
		$.errorMsgText.setText(msg);
	}else{
		//Hide error
		$.errorMsgView.setVisible(false);
		$.errorMsgView.setHeight("0dp");
		$.errorMsgText.setText("");
	}
};

Ti.App.addEventListener("view:redeemForm", function(e) {
	Ti.API.info("fired event view:redeemForm ------Captured");
	RedeemFormData=e;
	toggleErrorMsg("");
});



function deliverySubmit(){
	toggleErrorMsg("");
	var formValid = true;
	var errorMsg ="";
	var nameField = $.nameField.getValue();
	var surnameField = $.surnameField.getValue();
	var mobileNField = $.mobileNField.getValue();
	var addressField = $.addressField.getValue();
	var streetNField = $.streetNField.getValue();
	var cityField = $.cityField.getValue();
	var postCodeField = $.postCodeField.getValue();
	//TODO: need to add checks here 
	if(nameField=="")
	{
		errorMsg ="Invalid input: Name";
		formValid=false;
	}
	
	if(surnameField=="")
	{
		if(formValid){
			errorMsg ="Invalid input: Surname";
			formValid=false;
		}
	}
	
	if(addressField=="")
	{
		if(formValid){
			errorMsg="Invalid input: Street";
			formValid=false;
		}
	}
	
	if(streetNField=="")
	{
		if(formValid){
			errorMsg="Invalid input: Street Number";
			formValid=false;
		}
	}
	
	if(cityField=="")
	{
		if(formValid){
			errorMsg="Invalid input: City";
			formValid=false;
		}
	}
	
	if(postCodeField=="")
	{
		if(formValid){
			errorMsg="Invalid input: PostCode";
			formValid=false;
		}
	}
	
	if(mobileNField=="")
	{
		if(formValid){
			errorMsg="Invalid input: Mobile Number";
			formValid=false;
		}
	}
	
	if(formValid){
		$.nameField.blur();
		$.surnameField.blur();
		$.mobileNField.blur();
		$.addressField.blur();
		$.streetNField.blur();
		$.cityField.blur();
		$.postCodeField.blur();
		
		RedeemFormData.nameField = nameField;
		RedeemFormData.surnameField = surnameField;
		RedeemFormData.mobileNField = mobileNField;
		RedeemFormData.addressField =addressField;
		RedeemFormData.streetNField = streetNField;
		RedeemFormData.cityField = cityField;
		RedeemFormData.postCodeField = postCodeField;
		
		Ti.App.fireEvent("changeView", 
				{row : {name:"Review",viewsrc:"reviewRedeemForm",props:RedeemFormData,scroll:false}}
	    );
	 }else{
	 	toggleErrorMsg(errorMsg);
	 }
}



