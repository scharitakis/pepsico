/*Ti.App.addEventListener("sliderToggled", function(e) {
if (e.hasSlided) {
//	$.table.touchEnabled = false;
}
else {
//	$.table.touchEnabled = true;
}
});*/

var indicator=false;

function toggleIndicator(){
	if(indicator)
	{
		$.disableViewBgLoadingIndicator.setVisible(false);
		$.activityIndicator.hide();
		indicator=false;
	}else{
		$.disableViewBgLoadingIndicator.setVisible(true);
		if (Ti.Platform.osname === 'iphone'){		
			$.activityIndicator.setStyle(Titanium.UI.iPhone.ActivityIndicatorStyle.BIG);
		}else{
			$.activityIndicator.setStyle(Titanium.UI.ActivityIndicatorStyle.BIG);
		}
		$.activityIndicator.show();
		indicator=true;
	}
};

function toggleErrorMsg(msg){
	if(msg!="")
	{
		//Show error
		$.errorMsgView.setVisible(true);
		$.errorMsgView.setHeight(Titanium.UI.SIZE);
		$.errorMsgText.setText(msg);
	}else{
		//Hide error
		$.errorMsgView.setVisible(false);
		$.errorMsgView.setHeight("0dp");
		$.errorMsgText.setText("");
	}
};


function openPopupMsgView(e){
	//$.popUpView.setVisible(true);
	
}

function closePopupMsgView(e){
	//$.popUpView.setVisible(false);
	//$.loadingIndicator.setVisible(false);
	
}


// The first the view is visible check if we have a user
if(Ti.App.Properties.hasProperty('user')){
		$.msisdnField.setValue(Ti.App.Properties.getObject('user')['msisdn']);
		$.msisdnField.enabled=false;
}else{
	$.msisdnField.enabled=true;
}

// This will be triggered when
Ti.App.addEventListener("view:coupon", function(e) {
	Ti.API.info("fired event view:coupon ------Captured");
	Ti.API.info(e);
	toggleErrorMsg("");
	$.couponField.setValue("");
	if(Ti.App.Properties.hasProperty('user')){
		$.msisdnField.setValue(Ti.App.Properties.getObject('user')['msisdn']);
		$.msisdnField.enabled=false;
	}else{
		$.msisdnField.enabled=true;
		$.msisdnField.setValue("");
	}
});

function redeemSuccessClb(e){
	var data = JSON.parse(this.responseText);
	//alert(data);
	toggleIndicator();
	if(data.result=="SUCCESS")
	{
		if(data.isBigPrizeWinning){
				Ti.App.fireEvent("changeView", 
					{row : {name:"Winner",viewsrc:"winnerView",props:data,scroll:false}}
	    		);
	    }else{
    		Ti.App.fireEvent("changeView", 
				{row : {name:"No Winner",viewsrc:"nowinnerView",props:data,scroll:false}}
    		);
    	}
	}else{
		toggleErrorMsg(data.replyText);
	}
		
};

function redeemErrorClb(e){
	toggleIndicator();
	//alert("error"+JSON.stringify(e));
	toggleErrorMsg("A Problem occured when trying to Redeem. Please try again");
};


function sendPinSuccessClb(){
	var data = JSON.parse(this.responseText);
	toggleIndicator();
	if(data.result=="SUCCESS")
	{
		//We need to show to the user the form to put his/her pin
		Ti.App.fireEvent("changeView", 
			{row : {name:"Pin",viewsrc:"pinDialog",props:{user:false},scroll:false}}
    	);
	}
};


function sendPinErrorClb(){
	//alert(e);
	toggleErrorMsg("A Problem Occured. Please try again");
};

function msisdnCheckSuccessClb(e){
	var data = JSON.parse(this.responseText);
	var msisdn = $.msisdnField.getValue();
	//alert(data);
	if(!data.msisdnRegistered) // User not registered
	{
		// send the pin to user
		if(Alloy.Globals.online){
			var url = Alloy.Globals.serverUrl+"sendConfirmationCode?msisdn="+msisdn+"&confirmationChannel=MOBILE_APP";
			Alloy.Globals.userMsisdn = msisdn;
			Alloy.Globals.doGET(url,sendPinSuccessClb,sendPinErrorClb);
		};
	}else{
		//if user is registered then send the code
			if(Alloy.Globals.online){
				var msisdn = $.msisdnField.getValue();//79100000101
				var coupon = $.couponField.getValue();
				Alloy.Globals.userMsisdn = msisdn;
				var url =Alloy.Globals.serverUrl+"redeemCoupon?coupon="+coupon+"&msisdn="+msisdn+"&channel=MOBILE_APP&userAgent=MOBILE_APP&locale=null";
				Alloy.Globals.doGET(url,redeemSuccessClb,redeemErrorClb);
			};
		
	}
};

function msisdnCheckErrorClb(e){
	toggleIndicator();
	toggleErrorMsg("Server error");
	//alert("msisdnCheckErrorClb error"+ JSON.stringify(e));
};


function redeemCoupon(){
	//First Check if the MSISDN is Valid
	$.msisdnField.blur();
	$.couponField.blur();
	toggleErrorMsg("");
	if(Alloy.Globals.online){
		
		var msisdnReg = /^[0-9]{4,15}$/;
		var msisdn = $.msisdnField.getValue();//79100000101
		var coupon = $.couponField.getValue();
		var formValid=true;
		var errorMsg ="";
		if(msisdn=="")
		{
			errorMsg ="Please fill in you Mobile Phone";
			formValid=false;
		}else if(!msisdnReg.test(msisdn)){
			errorMsg ="Invalid input: MSISDN";
			formValid=false;
		}else if(coupon==""){
			errorMsg ="Please fill in the Coupon Code";
			formValid=false;
		}
		
		if(formValid){
			toggleIndicator();
			var url = Alloy.Globals.serverUrl + "isMsisdnRegistered?msisdn="+msisdn;
			Alloy.Globals.doGET(url,msisdnCheckSuccessClb,msisdnCheckErrorClb);
		}else{
			toggleErrorMsg(errorMsg);
		}
	}
}


function showLoginPage(){
	Ti.App.fireEvent("changeView", 
	{row : {name:"Login",viewsrc:"login",props:null,scroll:true}}
	);
}
