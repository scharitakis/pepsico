// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};


Alloy.Globals.online =Titanium.Network.online;

Alloy.Globals.userMsisdn ="";
if(Ti.App.Properties.hasProperty('user')){
	Alloy.Globals.userMsisdn=Ti.App.Properties.getObject('user')['msisdn'];
};

//Alloy.Globals.serverUrl = "http://10.130.35.77/promo/web/"; //Promo Staging
//Alloy.Globals.serverUrl = "http://10.1.6.29:8080/mgc/cgi/api/web/"; //Kostas pc mgc
Alloy.Globals.serverUrl = "http://10.1.3.40:8050/promo/web/"; //Kostas pc Promo





/*==========POST function===============*/
Alloy.Globals.doPost = function(data,url,loadCallBack,errCallBack){
	var xhr = Titanium.Network.createHTTPClient();
	//need to clear anyCookie saved
	//xhr.clearCookies(Alloy.Globals.hosturl);
	xhr.onload = loadCallBack;
	xhr.onerror = errCallBack;
	xhr.open("POST",url);
	//if(cookie){xhr.setRequestHeader("Cookie",cookie);}
	xhr.send(data);
};
/*==========POST function END===============*/

/*==========GET function===============*/
Alloy.Globals.doGET = function(url,loadCallBack,errCallBack){
	var xhr = Titanium.Network.createHTTPClient();
	//need to clear anyCookie saved
	//xhr.clearCookies(Alloy.Globals.hosturl);
	xhr.onload = loadCallBack;
	xhr.onerror = errCallBack;
	xhr.open("GET",url);
	//if(cookie){xhr.setRequestHeader("Cookie",cookie);}
	xhr.send();
};
/*==========GET function END===============*/